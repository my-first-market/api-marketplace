<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Review;
use Illuminate\Console\Command;

class CalculateRatingCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate_rating:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("start Calculating Rating");
        Product::where('active', true)->chunk(10, function ($items) {
            foreach ($items as $item) {
                $ratingReviews = Review::where('active', true)->where('item_id', $item->id)->pluck('rating');
                if ($ratingReviews->count() > 0) {
                    $sum = $ratingReviews->pipe(function ($ratingReviews){
                        return $ratingReviews->sum();
                    });

                    $item->update([
                        'rating' => $sum / $ratingReviews->count()
                    ]);
                }
            }
        });

        \Log::info("end Calculating Rating");
    }
}
