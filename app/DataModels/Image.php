<?php


namespace App\DataModels;


class Image
{
    private $src;

    public function __construct($src)
    {
        return $this->src = 'storage' . DIRECTORY_SEPARATOR . $src;
    }

    public function __get($name)
    {
        return $this->{$name};
    }
}
