<?php


namespace App\DataModels;


class QuerySort
{
    private $start = null;
    private $end = null;
    private $order = 'DESC';
    private $sort = 'id';

    public function __construct($params = [])
    {
        foreach ($params as $key => $value) {

            $key = str_ireplace('_', '', $key);

            $this->{$key} = $value;
        }
    }

    public function __get($name)
    {
        return $this->{$name};
    }
}
