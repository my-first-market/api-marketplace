<?php


namespace App\DataModels;


class SearchData
{
    private $items;
    private $firstPage = 1;
    private $lastPage;
    private $currentPage;

    /**
     * SearchData constructor.
     *
     * @param $params = [
     *                      $item
     *                      $firstPage
     *                      $LastPage
     *                      $currentPage
     *                  ]
     */
    public function __construct($params)
    {
        foreach ($params as $key => $value) {

            $this->{$key} = $value;
        }
    }

    public function __get($name)
    {
        return $this->{$name};
    }
}
