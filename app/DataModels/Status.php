<?php


namespace App\DataModels;


class Status
{
    public const CODE_PENDING   = 'pending';    // в ожидании
    public const CODE_ACTIVE    = 'active';     // активно
    public const CODE_DISABLED  = 'disabled';   // не активно
    public const CODE_DELETED   = 'deleted';    // удалено
    public const CODE_DECLINED  = 'declined';   // отказался
    public const CODE_CLOSED    = 'closed';     // закрыто
    public const CODE_MODERATED = 'moderated';  // проверено
    public const CODE_ERROR     = 'error';      // ошибка
    public const CODE_DRAFT     = 'draft';      // черновой вариант
    public const CODE_PAID      = 'paid';       // оплачено
    public const CODE_SENT      = 'sent';       // отправлен
    public const CODE_DELIVERED = 'delivered';  // доставлен

    public const CODE_TRUE      = true,
                 CODE_FALSE     = false;


}
