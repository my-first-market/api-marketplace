<?php

namespace App\Events;

use App\Models\User;
use App\Services\Mail\SendEmailInterface;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendEmailEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var SendEmailInterface
     */
    public $sendEmailService;

    /**
     * @var string
     */
    public $toEmail;

    /**
     * Create a new event instance.
     *
     * @param SendEmailInterface $sendEmailService
     * @param string $toEmail
     */
    public function __construct(SendEmailInterface $sendEmailService, string $toEmail)
    {
        $this->sendEmailService = $sendEmailService;
        $this->toEmail          = $toEmail;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
