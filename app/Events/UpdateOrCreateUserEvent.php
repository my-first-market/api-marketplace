<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UpdateOrCreateUserEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Request
     */
    private $request;
    private $user;


    /**
     * Create a new event instance.
     *
     * @param Request $request
     * @param $user
     */
    public function __construct(Request $request, $user)
    {
        $this->request = $request;
        $this->user    = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function __get($name)
    {
        return $this->{$name};
    }
}
