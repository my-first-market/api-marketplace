<?php

declare(strict_types=1);

namespace App\Extensions\SberbankAcquiring\Exception;

/**
 * Action exception.
 */
class ActionException extends SberbankAcquiringException
{
}
