<?php

declare(strict_types=1);

namespace App\Extensions\SberbankAcquiring\Exception;

/**
 * Network exception.
 */
class NetworkException extends SberbankAcquiringException
{
}
