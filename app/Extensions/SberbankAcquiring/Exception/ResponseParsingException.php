<?php

declare(strict_types=1);

namespace App\Extensions\SberbankAcquiring\Exception;

class ResponseParsingException extends SberbankAcquiringException
{
}
