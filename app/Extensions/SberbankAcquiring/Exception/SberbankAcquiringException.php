<?php

declare(strict_types=1);

namespace App\Extensions\SberbankAcquiring\Exception;

/**
 * Base exception.
 *
 */
class SberbankAcquiringException extends \Exception
{
}
