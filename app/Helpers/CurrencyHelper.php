<?php


namespace App\Helpers;


use App\Models\Currency;

class CurrencyHelper
{
    /**
     * Get amount in minimal unit
     *
     * @param $amount
     * @param $currencyCode
     *
     * @return int
     */
    public static function minUnitInCurrency($amount, $currencyCode)
    {
        return (int)($amount * Currency::$minUnitInCurrency[$currencyCode]);
    }
}
