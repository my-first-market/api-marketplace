<?php


namespace App\Helpers;


use App\Models\AcquiringTransaction;
use App\Models\Currency;
use App\Models\Image;
use App\Models\Order;
use App\Models\Product;
use App\Models\Role;
use App\Models\Shop;
use App\Models\User;

class ModelHelper
{
    /**
     * Sections is model table
     */
    const SECTION_ACQUIRING_TRANSACTION = 'acquiring_transactions',
          SECTION_CURRENCY              = 'currencies',
          SECTION_IMAGE                 = 'images',
          SECTION_ORDER                 = 'orders',
          SECTION_PRODUCT               = 'products',
          SECTION_ROLE                  = 'roles',
          SECTION_SHOP                  = 'shops',
          SECTION_USER                  = 'users';

    const SECTIONS = [
        self::SECTION_ACQUIRING_TRANSACTION => AcquiringTransaction::class,
        self::SECTION_CURRENCY              => Currency::class,
        self::SECTION_IMAGE                 => Image::class,
        self::SECTION_ORDER                 => Order::class,
        self::SECTION_PRODUCT               => Product::class,
        self::SECTION_ROLE                  => Role::class,
        self::SECTION_SHOP                  => Shop::class,
        self::SECTION_USER                  => User::class
    ];

    const RESOURCES = [
        //self::SECTION_ACQUIRING_TRANSACTION => AcquiringTransaction::class,
        //self::SECTION_CURRENCY              => Currency::class,
        //self::SECTION_IMAGE                 => Image::class,
        //self::SECTION_ORDER                 => Order::class,
        self::SECTION_PRODUCT               => \App\Http\Resources\Product::class,
        //self::SECTION_ROLE                  => Role::class,
        self::SECTION_SHOP                  => \App\Http\Resources\Shop::class,
        //self::SECTION_USER                  => User::class
    ];

    /**
     * Get model by Class or By Section
     *
     * @param $sectionOrTable
     *
     * @return mixed|null
     */
    public static function getClass($sectionOrTable)
    {
        return self::SECTIONS[$sectionOrTable] ?? null;
    }

    /**
     * Get Resource by model table
     *
     * @param $sectionOrTable
     *
     * @return mixed|null
     */
    public static function getResource($sectionOrTable)
    {
        return self::RESOURCES[$sectionOrTable] ?? null;
    }

}
