<?php


namespace App\Http\Controllers\Api\Acquiring;


use App\DataModels\Status;
use App\Http\Controllers\Api\BaseController;
use App\Models\AcquiringTransaction;
use App\Models\Order;
use App\Services\Acquiring\SberbankAcquiringService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class AcquiringController extends BaseController
{
    /**
     * Обработка успешной оплаты
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function success(Request $request)
    {
        try {
            SberbankAcquiringService::success($request);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        return Redirect::to(config('acquiring.redirect.success'));
    }

    /**
     * Обработка ошибки при оплате
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fail(Request $request)
    {
        try {
            SberbankAcquiringService::failed($request);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        return Redirect::to(config('acquiring.redirect.fails'));
    }
}
