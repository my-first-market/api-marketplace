<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Models\Order\Order;
use App\Models\Order\OrderProduct;
use App\Models\Product;
use App\Models\User;
use App\Models\InfoContact;
use Illuminate\Http\Request;
use SimpleXMLElement;

class ExportOrderController extends BaseController
{
    public function setMiddleware()
    {
        //$this->middleware('auth:api')->only(['store', 'csv', 'images']);
        //$this->middleware('check.admin:api');
    }

    public function xml(Request $request)
    {
        $orders = Order::all();
        //var_dump($orders);
        $ordersXML = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"standalone="yes"?><Заказы></Заказы>');        //$newsXML->addAttribute('newsPagePrefix', 'value goes here');

        foreach ($orders as $order) {
          $row = $ordersXML->addChild('Заказ');
          $row->addChild('Номер', $order->id);
          $row->addChild('СтоимостьЗаказа', $order->total_price);
          // $row->addChild('Валюта', 'RUR');
          //$row->addAttribute('id', $order->id);
          //$row->addAttribute('user_id', $order->user_id);
          //$row->addAttribute('total_pri', $order->id);
          //$row->addAttribute()
          $contact = InfoContact::where('section', 'order')->where('item_id', $order->id)->first();
          $row->addChild('Клиент', $contact);

          $user = User::find($order->user_id);
          if (!empty($user)) {
              $row->addChild('Клиент', $user->name);
              // $userRow->addChild('Клиент', $user->name);
              //$userRow->addAttribute('Имя', $user->name);
              //$userRow->addAttribute('Почта', $user->email);
          }


          $orderProducts = OrderProduct::where('order_id', $order->id)->get();
          //var_dump($orderProducts->first());
          if (!empty($orderProducts)) {
              $orderProductsRow = $row->addChild('Заказ');

              foreach($orderProducts as $orderProduct) {
                  $product = Product::find($orderProduct->product_id);
                  if (empty($product)) {
                      continue;
                  }
                  $sum = $orderProduct->current_amount * $orderProduct->current_price;
                  $orderProductRowChild = $orderProductsRow->addChild('СоставЗаказа');
                  // $orderProductRowChild->addChild('НомерТовара', $product->id);
                  $orderProductRowChild->addChild('Изделие', $product->name);
                  $orderProductRowChild->addChild('Количество', $orderProduct->current_amount);
                  $orderProductRowChild->addChild('Цена', $orderProduct->current_price);
                  $orderProductRowChild->addChild('Сумма', $sum);

              }
              
          }

        }
        //$newsIntro = $newsXML->addChild('content');
        //$newsIntro->addAttribute('type', 'latest');
        //Header('Content-type: text/xml');
        //echo $newsXML->asXML();
        return response($ordersXML->asXML(), 200, ['Content-Type' => 'application/xml']);
      //return $this->sendResponse([], 'Import product successfully.');
    }
}
