<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\BaseController;
use App\Models\Order\Order;
use App\Models\Order\OrderProduct;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use SimpleXMLElement;

class ExportOrderController extends BaseController
{
    public function setMiddleware()
    {
        //$this->middleware('auth:api')->only(['store', 'csv', 'images']);
        //$this->middleware('check.admin:api');
    }

    public function xml(Request $request)
    {
        $orders = Order::all();
        //var_dump($orders);
        $ordersXML = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"standalone="yes"?><Заказы></Заказы>');
        //$newsXML->addAttribute('newsPagePrefix', 'value goes here');

        foreach ($orders as $order) {
          $row = $ordersXML->addChild('Заказ');
          $row->addChild('Номер заказа', $order->id);
          $row->addChild('Стоимость заказа', $order->total_price);
          $row->addChild('Валюта', 'RUR');
          //$row->addAttribute('id', $order->id);
          //$row->addAttribute('user_id', $order->user_id);
          //$row->addAttribute('total_pri', $order->id);
          //$row->addAttribute()

          $user = User::find($order->user_id);
          if (!empty($user)) {
              $userRow = $row->addChild('Пользователь');
              $userRow->addChild('Имя', $user->name);
              $userRow->addChild('Почта', $user->email);
              //$userRow->addAttribute('Имя', $user->name);
              //$userRow->addAttribute('Почта', $user->email);
          }

          $orderProducts = OrderProduct::where('order_id', $order->id)->get();
          //var_dump($orderProducts->first());
          if (!empty($orderProducts)) {
              $orderProductsRow = $row->addChild('Товары');

              foreach($orderProducts as $orderProduct) {
                  $product = Product::find($orderProduct->product_id);
                  if (empty($product)) {
                      continue;
                  }

                  $orderProductRowChild = $orderProductsRow->addChild('Товар');
                  $orderProductRowChild->addChild('НомерТовара', $product->id);
                  $orderProductRowChild->addChild('НаименованиеТовара', $product->name);
                  $orderProductRowChild->addChild('ОписаниеТовара', $product->description);
              }
              
          }


        }
        //$newsIntro = $newsXML->addChild('content');
        //$newsIntro->addAttribute('type', 'latest');
        //Header('Content-type: text/xml');
        //echo $newsXML->asXML();
        return response($ordersXML->asXML(), 200, ['Content-Type' => 'application/xml']);
      //return $this->sendResponse([], 'Import product successfully.');
    }
}