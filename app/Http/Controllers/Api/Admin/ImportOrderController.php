<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\BaseController;
use App\Services\Import\ImportProductService;
use Illuminate\Http\Request;

class ImportOrderController extends BaseController
{
    public function setMiddleware()
    {
        $this->middleware('auth:api')->only(['store', 'csv', 'images']);
        $this->middleware('check.admin:api');
    }

    public function store(Request $request)
    {
        try {
            ImportProductService::import($request);
            return $this->sendResponse([], 'Import product successfully.');
        } catch (\Exception $e) {
            return  $this->sendError('Error delete row ', [$e->getMessage() . ' ' . $e->getTraceAsString()]);
        }
    }

    public function csv(Request $request)
    {
        try {
            $csv = ImportProductService::csv($request);
            return $this->sendResponse((object)['csv' => $csv], 'csv successfully.');
        } catch (\Exception $e) {
            return  $this->sendError('Error delete row ', [$e->getMessage() . ' ' . $e->getTraceAsString()]);
        }
    }

    public function images(Request $request)
    {
        try {
            $images = ImportProductService::images($request);
            return $this->sendResponse((object)['images' => $images], 'images successfully.');
        } catch (\Exception $e) {
            return  $this->sendError('Error delete row ', [$e->getMessage() . ' ' . $e->getTraceAsString()]);
        }
    }
}
