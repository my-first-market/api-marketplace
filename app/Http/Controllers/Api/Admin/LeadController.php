<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\AdminController;
use App\Services\Crud\Lead\LeadDestroyService;
use App\Services\Crud\Lead\LeadIndexService;
use App\Services\Crud\Lead\LeadShowService;
use App\Services\Crud\Lead\LeadStoreService;
use App\Services\Crud\Lead\LeadUpdateService;

class LeadController extends AdminController
{
    protected function setServices()
    {
        $this->indexService  = new LeadIndexService();
        $this->storeService  = new LeadStoreService();
        $this->showService   = new LeadShowService();
        $this->updateService = new LeadUpdateService();
        $this->deleteService = new LeadDestroyService();
    }
}
