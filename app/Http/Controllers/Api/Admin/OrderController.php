<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\AdminController;
use App\Services\Crud\Order\OrderDestroyService;
use App\Services\Crud\Order\OrderIndexService;
use App\Services\Crud\Order\OrderShowService;
use App\Services\Crud\Order\OrderStoreService;
use App\Services\Crud\Order\OrderUpdateService;

class OrderController extends AdminController
{
    protected function setServices()
    {
        $this->indexService  = new OrderIndexService();
        $this->storeService  = new OrderStoreService();
        $this->showService   = new OrderShowService();
        $this->updateService = new OrderUpdateService();
        $this->deleteService = new OrderDestroyService();
    }
}
