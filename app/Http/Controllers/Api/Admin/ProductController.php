<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\AdminController;
use App\Services\Crud\Product\ProductDestroyService;
use App\Services\Crud\Product\ProductIndexService;
use App\Services\Crud\Product\ProductShowService;
use App\Services\Crud\Product\ProductStoreService;
use App\Services\Crud\Product\ProductUpdateService;

class ProductController extends AdminController
{
    protected function setServices()
    {
        $this->indexService  = new ProductIndexService();
        $this->storeService  = new ProductStoreService();
        $this->showService   = new ProductShowService();
        $this->updateService = new ProductUpdateService();
        $this->deleteService = new ProductDestroyService();
    }

    /**
     * @OA\Get(
     *      path="/api/products",
     *      operationId="index",
     *      tags={"Products"},
     *      summary="Получить список магазинов",
     *      description="Возвращает список магазинов"
     *     )
     *
     * @OA\Get(
     *      path="/api/products/{id}",
     *      operationId="show",
     *      tags={"Products"},
     *      summary="Получить конекретный магазин",
     *      description="Returns list of projects"
     *     )
     *
     * @OA\Post(
     *      path="/api/products",
     *      operationId="create",
     *      tags={"Products"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects"
     *     )
     *
     * @OA\Puth(
     *      path="/api/products/{id}",
     *      operationId="update",
     *      tags={"Products"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects"
     *     )
     *
     * @OA\Delete(
     *      path="/api/products/{id}",
     *      operationId="delete",
     *      tags={"Products"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects"
     *     )
     *
     */
}
