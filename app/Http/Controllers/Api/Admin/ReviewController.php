<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\AdminController;
use App\Services\Crud\Review\ReviewDestroyService;
use App\Services\Crud\Review\ReviewIndexService;
use App\Services\Crud\Review\ReviewShowService;
use App\Services\Crud\Review\ReviewStoreService;
use App\Services\Crud\Review\ReviewUpdateService;

class ReviewController extends AdminController
{
    protected function setServices()
    {
        $this->indexService  = new ReviewIndexService();
        $this->storeService  = new ReviewStoreService();
        $this->showService   = new ReviewShowService();
        $this->updateService = new ReviewUpdateService();
        $this->deleteService = new ReviewDestroyService();
    }

    /**
     * @OA\Get(
     *      path="/api/reviews",
     *      operationId="index",
     *      tags={"Reviews"},
     *      summary="Получить список магазинов",
     *      description="Возвращает список магазинов"
     *     )
     *
     * @OA\Get(
     *      path="/api/reviews/{id}",
     *      operationId="show",
     *      tags={"Reviews"},
     *      summary="Получить конекретный магазин",
     *      description="Returns list of projects"
     *     )
     *
     * @OA\Post(
     *      path="/api/reviews",
     *      operationId="create",
     *      tags={"Reviews"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects"
     *     )
     *
     * @OA\Puth(
     *      path="/api/reviews/{id}",
     *      operationId="update",
     *      tags={"Reviews"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects"
     *     )
     *
     * @OA\Delete(
     *      path="/api/reviews/{id}",
     *      operationId="delete",
     *      tags={"Reviews"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects"
     *     )
     *
     */
}
