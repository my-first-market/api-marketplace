<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\AdminController;
use App\Services\Crud\Shop\ShopDestroyService;
use App\Services\Crud\Shop\ShopIndexService;
use App\Services\Crud\Shop\ShopShowService;
use App\Services\Crud\Shop\ShopStoreService;
use App\Services\Crud\Shop\ShopUpdateService;
use Illuminate\Http\Request;

class ShopController extends AdminController
{
    protected function setServices()
    {
        $this->indexService  = new ShopIndexService();
        $this->storeService  = new ShopStoreService();
        $this->showService   = new ShopShowService();
        $this->updateService = new ShopUpdateService();
        $this->deleteService = new ShopDestroyService();
    }

    /**
     * @OA\Get(
     *      path="/api/shops",
     *      operationId="index",
     *      tags={"Shops"},
     *      summary="Получить список магазинов",
     *      description="Возвращает список магазинов",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="Http/Resorces/Shop")
     *      )
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     *
     * @OA\Get(
     *      path="/api/shops/{id}",
     *      operationId="show",
     *      tags={"Shops"},
     *      summary="Получить конекретный магазин",
     *      description="Returns list of projects",
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="Http/Resorces/Shop")
     *      )
     *     )
     *
     * @OA\Post(
     *      path="/api/shops",
     *      operationId="create",
     *      tags={"Shops"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects",
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="Http/Resorces/Shop")
     *      )
     *     )
     *
     * @OA\Puth(
     *      path="/api/shops/{id}",
     *      operationId="update",
     *      tags={"Shop"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects",
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="Http/Resorces/Shop")
     *      )
     *     )
     *
     * @OA\Delete(
     *      path="/api/shops/{id}",
     *      operationId="delete",
     *      tags={"Shops"},
     *      summary="Создание нового магазина",
     *      description="Returns list of projects",
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="Http/Resorces/Shop")
     *      )
     *     )
     *
     */
}
