<?php


namespace App\Http\Controllers\Api\Admin;


use App\Http\Controllers\Api\AdminController;
use App\Services\Crud\User\UserDestroyService;
use App\Services\Crud\User\UserIndexService;
use App\Services\Crud\User\UserShowService;
use App\Services\Crud\User\UserStoreService;
use App\Services\Crud\User\UserUpdateService;

class UserController extends AdminController
{
    protected function setServices()
    {
        $this->indexService  = new UserIndexService();
        $this->storeService  = new UserStoreService();
        $this->showService   = new UserShowService();
        $this->updateService = new UserUpdateService();
        $this->deleteService = new UserDestroyService();
    }
}
