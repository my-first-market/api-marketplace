<?php


namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AdminController extends BaseController
{
    /**
     * @OA\OpenApi(
     *     @OA\Info(
     *         version="1.0.0",
     *         title="Marketplace",
     *         @OA\License(name="MIT")
     *     ),
     *     @OA\Server(
     *         description="Api server",
     *         url="petstore.swagger.io",
     *     ),
     * )
     */

    protected $indexService;
    protected $storeService;
    protected $showService;
    protected $updateService;
    protected $deleteService;

    public function setMiddleware()
    {
        $this->middleware('auth:api')->only(['index', 'show', 'store', 'update', 'destroy']);
        $this->middleware('check.admin:api');
    }

    public function index(Request $request)
    {
        try {
            $this->indexService->init($request);
            $result = $this->indexService->getResult();
            $meta = $this->indexService->getMeta();
            return $this->sendResponseWithMeta($result, $meta);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->sendError('Error index', [$e->getMessage() . ' ' . $e->getTraceAsString()]);
        }
    }

    public function store(Request $request)
    {
        try {
            $result = $this->storeService->getResult($request);
            return $this->sendResponse($result, 'Successful');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->sendError('Error store', [$e->getMessage() . ' ' . $e->getTraceAsString()]);
        }
    }

    public function show($id)
    {
        try {
            $result = $this->showService->getResult($id);
            return $this->sendResponse($result, 'Successful');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->sendError('Error show', [$e->getMessage() . ' ' . $e->getTraceAsString()]);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $result = $this->updateService->getResult($request, $id);
            return $this->sendResponse($result, 'Successful');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->sendError('Error update', [$e->getMessage() . ' ' . $e->getTraceAsString()]);
        }
    }

    public function destroy($id)
    {
        try {
            $this->deleteService->destroy($id);
            return $this->sendResponse([], 'Row deleted successfully.');
        } catch (\Exception $e) {
            return  $this->sendError('Error delete row ', [$e->getMessage() . ' ' . $e->getTraceAsString()]);
        }
    }
}
