<?php


namespace App\Http\Controllers\Api\Auth;


use App\DataModels\Status;
use App\Http\Controllers\Api\BaseController;
use App\Models\Lead;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Ramsey\Uuid\Uuid;

class LeadController extends BaseController
{
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'c_password' => 'required|same:password',

                //'email' => 'required',
                'phone' => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()) throw new ValidationException($validator);

            $user     = Auth::user();
            $password = $request['password'] ?? Uuid::uuid4();
            $name     = $request['name'] ?? 'seller' . random_int(111111111, 999999999);

            if (empty($user)) {
                $user = User::create([
                    'name' => $name,
                    'email' => $request['email'],
                    'password' => bcrypt($password)
                ]);

                $role = Role::where('code', Role::ROLE_SELLER)->first();

                UserRole::create([
                    'user_id' => $user->id,
                    'role_id' => $role->id
                ]);
            } else {
                $role = UserRole::where('user_id', $user->id)->first();
                $role->role_id = (Role::where('code', Role::ROLE_SELLER)->first())->id;
                $role->save();
            }

            Lead::create([
                'active'      => Status::CODE_FALSE,
                'email'       => $request['email'],
                'phone'       => $request['phone'],
                'description' => $request['description'],
                'user_id'     => $user->id
            ]);

            $messageTest = '<p>Ваша заявка на создание аккаунта продавца принята</p>';
            $messageTest .= '<p>Если вы забыли пароль, то <a href="https://api.myeden.xyz/password/reset">нажмите здесь</a></p>';
            \Mail::to($user->email)->send(new \App\Mail\RegisterMail('Поздравляем!', $messageTest, 'Создание аккаунта продавца на myeden.xyz'));

            return $this->sendResponse('ok', 'ok');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), [$e->getTraceAsString()]);
        }
    }
}
