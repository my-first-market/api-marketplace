<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\User\User as UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends BaseController
{
    /**
     * Login api
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();

            if (!$user->active) return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);

            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['user'] =  new UserResource($user);

            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }

    /**
     * User logout
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Auth::logout();

        return $this->sendResponse('User logout');
    }
}
