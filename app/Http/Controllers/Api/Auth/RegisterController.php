<?php


namespace App\Http\Controllers\Api\Auth;


use App\DataModels\Status;
use App\Events\SendEmailEvent;
use App\Http\Controllers\Api\BaseController;
use App\Listeners\SendEmailListener;
use App\Models\Role;
use App\Models\User;
use App\Http\Resources\User\User as UserResource;
use App\Models\UserRole;
use App\Services\Mail\RegisterMailService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            //TODO вынести в сервис
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $input['active'] = true;
            $user = User::create($input);

            $role = Role::where('code', Role::ROLE_USER)->first();

            UserRole::create([
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);

            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['user']  =  new UserResource($user);

            event(new SendEmailEvent(new RegisterMailService(), $user->email));

        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), [$e->getTraceAsString()]);
        }

        return $this->sendResponse($success, 'User register successfully.');
    }
}
