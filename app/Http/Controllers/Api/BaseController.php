<?php


namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
    public function __construct()
    {
        $this->setMiddleware();
        $this->setServices();
    }

    /**
     * Set middleware
     */
    protected function setMiddleware()
    {

    }

    protected function setServices()
    {

    }

    /**
     * success response method.
     *
     * @param $result
     * @param $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result = null, $message = 'ok')
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    /**
     * success response with meta
     *
     * @param $result
     * @param $meta
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponseWithMeta($result, $meta)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'meta'    => $meta,
            'message' => 'ok',
        ];


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @param $error
     * @param array $errorMessages
     * @param int $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    public function sendFailsValidator($validatorError)
    {
        return $this->sendError('Validation Error. ', $validatorError);
    }
}
