<?php


namespace App\Http\Controllers\Api\Client;


use App\DataModels\QuerySort;
use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Client\OrderResource;
use App\Models\Order\Order;
use App\Services\Client\CreateOrder;
use App\Services\Update\OrderProductUpdateService as OrderProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends BaseController
{
    const DEFAULT_PAGINATE = 7;

    public function setMiddleware()
    {
        $this->middleware('auth:api')->only(['index', 'show', 'store', 'update', 'append']);
    }

    /**
     * Get orders
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $orderQuery = Order::querySort(new QuerySort($request->all()));

        $user = Auth::user();

        $orderQuery = $orderQuery->where('user_id', $user->id);

        $paginate   = $orderQuery->paginate(
            $request->perPage ?? self::DEFAULT_PAGINATE,
            ['*'],
            '',
            $request->page ?? 1
        );

        $meta = (object)[
            'page'     => (int)$paginate->currentPage(),
            'perPage'  => (int)$paginate->perPage(),
            'total'    => (int)Order::where('user_id', $user->id)->count()
        ];

        return $this->sendResponseWithMeta(OrderResource::collection(collect($paginate->items())), $meta);
    }

    /**
     * Create orders
     * post method orders
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $resultStore = CreateOrder::store($request);
            $result      = [
                'orders'        => OrderResource::collection(collect($resultStore->getOrders())),
                'acquiring_url' => $resultStore->getUrl()
            ];

            return $this->sendResponse($result, 'Order created successfully.');
        } catch (\Exception $e) {
            if (!empty($orders)) {
                foreach ($orders as $order) {
                    $order->products()->delete();
                    $order->delete();
                }
            }
            return $this->sendError('Error update product', [$e->getMessage(), $e->getTraceAsString()]);
        }
    }

    /**
     * Get orders
     * get method - orders/{id}
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = Auth::user();

        $order = Order::where('id', $id)->where('user_id', $user->id)->first();

        if (is_null($order)) return $this->sendError('Not find orders');

        return $this->sendResponse(new OrderResource($order));
    }

    /**
     * Update orders
     * put method - orders/{id}
     *
     * @param Request $request
     * @param Order $order
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Order $order)
    {
        return $this->sendError('Error update order');

        try {

            (new OrderProductService($request, $order))->update();

            return $this->sendResponse(new OrderResource($order));
        } catch (\Exception $e) {
            return $this->sendError('Error update order', [$e->getMessage()]);
        }
    }
}
