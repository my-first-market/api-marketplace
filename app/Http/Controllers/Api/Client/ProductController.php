<?php


namespace App\Http\Controllers\Api\Client;


use App\DataModels\QuerySort;
use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Client\ProductResource;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    const DEFAULT_PAGINATE = 20;

    /**
     * Get products
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $productQuery = Product::querySort(new QuerySort($request->all()));
        $productQuery = Product::querySearch($productQuery, $request);
        $shopIds      = Shop::where('active', true)->pluck('id')->toArray();
        $paginate     = $productQuery->where('active', true)->whereIn('shop_id', $shopIds)
            ->paginate(
            $request->perPage ?? self::DEFAULT_PAGINATE,
            ['*'],
            '',
            $request->page ?? 1
        );

        $meta = (object)[
            'page'     => (int)$paginate->currentPage(),
            'perPage'  => (int)$paginate->perPage(),
            'total'    => (int)(Product::querySearch(Product::where('active', true)
                ->whereIn('shop_id', $shopIds), $request))
                ->count(),
            'minPrice' => (float)Product::min('price'),
            'maxPrice' => (float)Product::max('price')
        ];

        return $this->sendResponseWithMeta(ProductResource::collection(collect($paginate->items())), $meta);
    }

    /**
     * Get product
     * get method - products/{id}
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $shopIds = Shop::where('active', true)->pluck('id')->toArray();
        $product = Product::where('active', true)->whereIn('shop_id', $shopIds)->where('id', $id)->first();

        if (is_null($product)) return $this->sendError('Not find product');

        return $this->sendResponse(new ProductResource($product));
    }
}
