<?php


namespace App\Http\Controllers\Api\Client;


use App\DataModels\QuerySort;
use App\DataModels\Status;
use App\Helpers\ModelHelper;
use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Client\OrderResource;
use App\Http\Resources\Client\ReviewResource;
use App\Models\Order\Order;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ReviewController extends BaseController
{
    const DEFAULT_PAGINATE = 5;

    public function setMiddleware()
    {
        $this->middleware('auth:api')->only(['index', 'show', 'store', 'update']);
    }

    public function index(Request $request)
    {
        $query = Review::querySort(new QuerySort($request->all()));

        $query = $query->where('active', Status::CODE_TRUE)
            ->where('section', ModelHelper::SECTION_PRODUCT)
            ->where('item_id', $request['product_id'] ?? 0);

        $paginate  = $query->paginate(
            $request->perPage ?? self::DEFAULT_PAGINATE,
            ['*'],
            '',
            $request->page ?? 1
        );

        $meta = (object)[
            'page'     => (int)$paginate->currentPage(),
            'perPage'  => (int)$paginate->perPage(),
            'total'    => (int)$query->count()
        ];

        return $this->sendResponseWithMeta(ReviewResource::collection(collect($paginate->items())), $meta);
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'product_id' => 'required|integer',
                'comment'    => 'required',
                'rating'     => 'required|integer',
            ]);

            if ($validator->fails()) throw new ValidationException($validator);

            $user = Auth::user();

            $review = Review::create([
                'active'  => Status::CODE_FALSE,
                'section' => ModelHelper::SECTION_PRODUCT,
                'item_id' => $request['product_id'],
                'user_id' => $user->id,
                'comment' => $request['comment'],
                'rating'  => $request['rating']
            ]);

            return $this->sendResponse(new ReviewResource($review), 'Reviews created successfully.');

        } catch (\Exception $e) {
            return $this->sendError('Error create review', [$e->getMessage(), $e->getTraceAsString()]);
        }
    }
}
