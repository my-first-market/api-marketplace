<?php


namespace App\Http\Controllers\Api\Client;


use App\Http\Controllers\Api\BaseController;
use App\Services\Search\SearchModelService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SearchController extends BaseController
{
    public function search(Request $request)
    {
        try {
            $result = (new SearchModelService())->getResult($request);
            return $this->sendResponse($result, 'Successful');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->sendError('Error search', [$e->getMessage()]);
        }
    }
}
