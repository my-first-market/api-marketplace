<?php


namespace App\Http\Controllers\Api\Client;


use App\DataModels\QuerySort;
use App\Http\Controllers\Api\BaseController;
use App\Models\Shop;
use App\Http\Resources\Shop as ShopResource;
use Illuminate\Http\Request;

class ShopController extends BaseController
{
    const DEFAULT_PAGINATE = 20;

    /**
     * Get shops
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $shopQuery  = Shop::querySort(new QuerySort($request->all()));
        $shopQuery  = Shop::querySearch($shopQuery, $request);
        $paginate   = $shopQuery->where('active', true)->paginate(
            $request->perPage ?? self::DEFAULT_PAGINATE,
            ['*'],
            '',
            $request->page ?? 1
        );

        $meta = (object)[
            'page'     => (int)$paginate->currentPage(),
            'perPage'  => (int)$paginate->perPage(),
            'total'    => (int)(Shop::querySearch(Shop::where('active', true), $request))->count()
        ];

        return $this->sendResponseWithMeta(ShopResource::collection(collect($paginate->items())), $meta);
    }

    /**
     * Get shop
     * get method - shops/{id}
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $shop = Shop::where('active', true)->where('id',$id)->first();

        if (is_null($shop)) return $this->sendError('Not find shop');

        return $this->sendResponse(new ShopResource($shop));
    }
}
