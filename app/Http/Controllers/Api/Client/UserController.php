<?php


namespace App\Http\Controllers\Api\Client;


use App\Events\UpdateOrCreateUserEvent;
use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\User\User as UserResource;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    public function setMiddleware()
    {
        $this->middleware('auth:api')->only(['show', 'update', 'destroy']);
    }

    /**
     * Get user
     * get method - users/{id}
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = User::find($id);

        if ((Auth::user())->id !== $user->id) {
            return $this->sendError('Access denied');
        }

        if (is_null($user)) return $this->sendError('Not find user');

        return $this->sendResponse(new UserResource($user));
    }

    /**
     * Update user
     * put method - users/{id}
     *
     * @param Request $request
     * @param User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        if ((Auth::user())->id !== $user->id) {
            return $this->sendError('Access denied');
        }

        $input = $request->all();

        try {

            if (!empty($input['password'])) {
                $input['password'] = bcrypt($input['password']);
            }

            $user->update($input);

            event(new UpdateOrCreateUserEvent($request, $user));

            return $this->sendResponse(new UserResource($user));
        } catch (\Exception $e) {
            return $this->sendError('Error update user', [$e->getMessage()]);
        }
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        if ((Auth::user())->id !== $user->id) {
            return $this->sendError('Access denied');
        }

        try {
            Auth::logout();
            $user->delete();
            return $this->sendResponse([], 'User deleted successfully.');
        } catch (\Exception $e) {
            return  $this->sendError('Error delete user');
        }
    }
}
