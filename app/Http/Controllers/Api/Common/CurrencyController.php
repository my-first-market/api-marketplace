<?php


namespace App\Http\Controllers\Api\Common;


use App\DataModels\QuerySort;
use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Currency as CurrencyResource;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyController extends BaseController
{
    public function setMiddleware()
    {
        $this->middleware('auth:api')->only(['store', 'update', ]);
        $this->middleware('check.admin:api')->only(['store', 'update', ]);
    }

    /**
     * Get currencies
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $currencyQuery = Currency::querySort(new QuerySort($request->all()));

        if (empty($request->page)) {
            $currencies = $currencyQuery->get();
        } else {
            $currencies = $currencyQuery->paginate(50, ['*'], $request->page);
        }

        $meta = (object)['total' => Currency::all()->count()];

        return $this->sendResponseWithMeta(CurrencyResource::collection($currencies), $meta);
    }

    /**
     * Create currencies
     * post method currencies
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        unset($input['include']);

        $validator = Validator::make($input, [
            'name' => 'required|unique:currencies',
            'code' => 'required|unique:currencies'
        ]);

        if ($validator->fails()) return $this->sendFailsValidator($validator->errors());

        $currency = Currency::create($input);

        return $this->sendResponse(new CurrencyResource($currency), 'Currency created successfully.');
    }

    /**
     * Get currencies
     * get method - currencies/{id}
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $currency = Currency::find($id);

        if (is_null($currency)) return $this->sendError('Not find currency');

        return $this->sendResponse(new CurrencyResource($currency));
    }

    /**
     * Update currencies
     * put method - currencies/{id}
     *
     * @param Request $request
     * @param Currency $currency
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Currency $currency)
    {
        $input = $request->all();
        unset($input['include']);

//        $validator = Validator::make($request->all(), [
//            'name' => 'unique:users,name,$this->id,id',
//            'email' => 'unique:users,email,$this->id,id'
//        ]);
//
//        if ($validator->fails()) return $this->sendFailsValidator($validator->errors());

        try {

            $currency->update($input);

            return $this->sendResponse(new CurrencyResource($currency));
        } catch (\Exception $e) {
            return $this->sendError('Error update currency', [$e->getMessage()]);
        }
    }

    /**
     * @param Currency $currency
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Currency $currency)
    {
        try {
            $currency->delete();
            return $this->sendResponse([], 'Currency deleted successfully.');
        } catch (\Exception $e) {
            return  $this->sendError('Error delete currency');
        }
    }
}
