<?php


namespace App\Http\Controllers\Api\Common;


use App\Http\Controllers\Api\BaseController;
use App\Services\FileSystem\Image as ImageService;
use App\Http\Resources\ImageUpload as ImageUploadResource;
use Illuminate\Http\Request;

class ImageController extends BaseController
{
    public function setMiddleware()
    {
        $this->middleware('auth:api');
        $this->middleware('check.admin:api');
    }

    public function upload(Request $request)
    {
        try {
            $images = ImageService::upload($request);

            return $this->sendResponse(ImageUploadResource::collection($images));
        } catch (\Exception $e) {
            \Log::error($e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return $this->sendError($e->getMessage());
        }
    }
}
