<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Imports\ProductsImport;
use App\Models\InfoContact;
use App\Models\User;
use App\Services\Acquiring\SberbankAcquiringService;
use App\Services\Update\InfoContactService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('create');
    }

    public function test(Request $request)
    {

    }

    public function create(Request $request)
    {

    }
}
