<?php


namespace App\Http\Middleware;

use App\Models\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRoleForAdmin
{
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();

        $roles = $user->roles()->pluck('code')->toArray();

        if (in_array(Role::ROLE_ADMIN, $roles) || in_array(Role::ROLE_SELLER, $roles)) {
            return $next($request);
        }

        abort(404);
    }
}
