<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\Client\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'current_weight' => $this->current_weight,
            'current_amount' => $this->current_amount,
            'current_price'  => $this->current_price,
            'description'    => $this->description,
            'product'        => new ProductResource($this->product),
        ];
    }
}
