<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\Currency as CurrencyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $contact = $this->contact()->first();

        return [
            'id'             => $this->id,
            'status'         => $this->status,
            'total_price'    => $this->total_price,
            'delivery_price' => $this->delivery_price,
            'currency'       => new CurrencyResource($this->currency),
            'products'       => OrderProductResource::collection($this->products),
            'phone_mobile'   => $contact ? $contact->phone_mobile : null,
            'country'        => $contact ? $contact->country : null,
            'city'           => $contact ? $contact->city : null,
            'address'        => $contact ? $contact->address : null,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
        ];
    }
}
