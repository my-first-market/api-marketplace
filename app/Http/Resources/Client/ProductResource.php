<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\Currency as CurrencyResource;
use App\Http\Resources\Image as ImageResource;
use App\Http\Resources\Shop as ShopResource;
use App\Http\Resources\ShopWithoutProduct as ShopForProductResource;
use App\Http\Resources\User\User as UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'rating'     => (int)$this->rating,
            'description' => $this->description,
            'html'        => $this->html,
            'weight'      => $this->weight,
            'amount'      => $this->amount,
            'price'       => $this->price,
            'shop'        => new ShopForProductResource($this->shop),
            'currency'    => new CurrencyResource($this->currency),
            'logo'        => $this->getImageSrc('logo'),
            'gallery'     => ImageResource::collection($this->imageQuery('gallery')->get()),
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'reviews'     => ReviewResource::collection($this->reviews),
        ];
    }
}
