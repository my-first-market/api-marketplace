<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\User\UserReviewResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'item_id'    => $this->item_id,
            'user'       => new UserReviewResource($this->user),
            'comment'    => $this->comment,
            'rating'     => $this->rating,
            'created_at' => $this->created_at
        ];
    }
}
