<?php

namespace App\Http\Resources\Client;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'items'       => $this->items,
            'firstPage'   => $this->firstPage,
            'lastPage'    => $this->lastPage,
            'currentPage' => $this->currentPage
        ];
    }
}
