<?php

namespace App\Http\Resources\Common;

use Illuminate\Http\Resources\Json\JsonResource;

class InfoContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'phone_mobile' => $this->phone_mobile,
            'phone_home'   => $this->phone_home,
            'country'      => $this->country,
            'city'         => $this->city,
            'address'      => $this->address
        ];
    }
}
