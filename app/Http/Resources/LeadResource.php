<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LeadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'active'      => $this->active,
            'email'       => $this->email,
            'phone'       => $this->phone,
            'description' => $this->description,
            'user'        => new User\User($this->user),
        ];
    }
}
