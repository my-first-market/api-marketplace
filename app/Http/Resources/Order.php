<?php

namespace App\Http\Resources;

use App\Http\Resources\Currency as CurrencyResource;
use App\Http\Resources\OrderProduct as OrderProductResource;
use App\Http\Resources\User\User as UserResource;
use App\Models\Acquiring\AcquiringOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $productIds = \App\Models\Order\OrderProduct::where('order_id', $this->id)->pluck('product_id')->toArray();
        $contact = $this->contact()->first();
        $acquiring = AcquiringOrder::where('order_id', $this->id)->first();
        $acquiring_id = $acquiring ? $acquiring->acquiring_id : null;
        return [
            'id'             => $this->id,
            'status'         => $this->status,
            'total_price'    => $this->total_price,
            'delivery_price' => $this->delivery_price,
            'currency'       => new CurrencyResource($this->currency),
            'user'           => new UserResource($this->user),
            'productIds'     => $productIds,
            'products'       => \App\Http\Resources\Client\OrderProductResource::collection($this->products),
            'phone_mobile'   => $contact ? $contact->phone_mobile : null,
            'country'        => $contact ? $contact->country : null,
            'city'           => $contact ? $contact->city : null,
            'address'        => $contact ? $contact->address : null,
            'description'    => $this->description ?? "",
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
            'acquiring_id'   => $acquiring_id,
        ];
    }
}
