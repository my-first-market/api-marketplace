<?php

namespace App\Http\Resources;

use App\Http\Resources\Currency as CurrencyResource;
use App\Http\Resources\ShopWithoutProduct as ShopForProductResource;
use App\Http\Resources\User\User as UserResource;
use App\Http\Resources\Image as ImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'active'      => (bool)$this->active,
            'rating'     => (int)$this->rating,
            'name'        => $this->name,
            'description' => $this->description,
            'html'        => $this->html,
            'user'        => new UserResource($this->user),
            'shop'        => new ShopForProductResource($this->shop),
            'weight'      => $this->weight,
            'amount'      => $this->amount,
            'price'       => $this->price,
            'currency'    => new CurrencyResource($this->currency),
            'logo'        => $this->getImageSrc('logo'),
            'gallery'     => ImageResource::collection($this->imageQuery('gallery')->get()),
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
        ];
    }
}
