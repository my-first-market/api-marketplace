<?php

namespace App\Http\Resources;

use App\Http\Resources\Client\ProductResource;
use App\Http\Resources\Common\InfoContactResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User\User as UserResource;

class Shop extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'active'      => (bool)$this->active,
            'name'        => $this->name,
            'description' => $this->description,
            'html'        => $this->html,
            'user'        => new UserResource($this->user),
            'logo'        => $this->getImageSrc('logo'),
            'products'    => ProductResource::collection($this->products),
            'contact'     => new InfoContactResource($this->contact()->first()),
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
        ];
    }
}
