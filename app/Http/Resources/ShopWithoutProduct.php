<?php

namespace App\Http\Resources;

use App\Http\Resources\Common\InfoContactResource;
use App\Http\Resources\ProductResource as ProductForShopResource;
use App\Http\Resources\User\User as UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopWithoutProduct extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'description' => $this->description,
            'html'        => $this->html,
            'user'        => new UserResource($this->user),
            'logo'        => $this->getImageSrc('logo'),
            'contact'     => new InfoContactResource($this->contact()->first()),
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
        ];
    }
}
