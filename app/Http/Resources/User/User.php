<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Common\InfoContactResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Role as RoleResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'active'    => (bool)$this->active,
            'name'      => $this->name,
            'email'     => $this->email,
            'logo'      => $this->getImageSrc('logo'),
            'role'      => $this->roles()->first() ? ($this->roles()->first())->code : \App\Models\Role::ROLE_USER,
            'contact'   => new InfoContactResource($this->contact()->first()),
        ];
    }
}
