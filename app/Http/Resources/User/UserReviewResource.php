<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Common\InfoContactResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'      => $this->name,
            'logo'      => $this->getImageSrc('logo'),
        ];
    }
}
