<?php

namespace App\Imports;

use App\DataModels\Status;
use App\Models\Currency;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $shop = request('shop');
        return new Product([
            'active'        => !empty($row['amount']) && $row['amount'] > 0 ? 1 : 0,
            'name'          => $row['name'],
            'description'   => $row['description'] ?? null,
            'html'          => $row['html'] ?? null,
            'user_id'       => Auth::id(),
            'amount'        => $row['amount'],
            'price'         => $row['price'],
            'shop_id'       => $shop['id'],
            'currency_id'   => (Currency::where('code', Currency::CODE_RUR)->first())->id
        ]);
    }
}
