<?php

namespace App\Listeners;


use App\Services\Update\ImageUpdateService as ImageService;
use App\Services\Update\InfoContactService;

class UpdateOrCreateUserListener
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        (new ImageService($event->request, $event->user))->update();
        (new InfoContactService($event->request, $event->user))->update();
    }
}
