<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $preheader;
    protected $content;
    protected $subjectText;

    /**
     * Create a new message instance.
     *
     * @param $preheader
     * @param $html
     * @param string $subject
     */
    public function __construct($preheader, $html, $subject)
    {
        $this->preheader   = $preheader;
        $this->content     = $html;
        $this->subjectText = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.mail-template', ['preheader' => $this->preheader, 'html' => $this->content])
            ->subject($this->subjectText);
    }
}
