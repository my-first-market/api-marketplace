<?php


namespace App\Models\Acquiring;


use App\Models\Model;

class Acquiring extends Model
{
    protected $table = 'acquiring';
    protected $guarded = [];
    public $incrementing = false;

    const DEFAULT_INCREMENT = 6661919480;
    const MIN_RAND_INCREMENT = 10;
    const MAX_RAND_INCREMENT = 1844;

    /**
     * Создание id защищенных от брутфорса
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     *
     * @throws \Exception
     */
    public static function create(array $attributes = [])
    {
        $acquiring = Acquiring::latest('id')->first();
        $lastAcquiringId = $acquiring ? $acquiring->id : self::DEFAULT_INCREMENT;
        $lastAcquiringId = $lastAcquiringId < self::DEFAULT_INCREMENT ? self::DEFAULT_INCREMENT : $lastAcquiringId;
        $attributes['id'] = $lastAcquiringId + random_int(self::MIN_RAND_INCREMENT, self::MAX_RAND_INCREMENT);

        return (new static)->newQuery()->create($attributes);
    }
}
