<?php


namespace App\Models;


use App\Traits\QueryTrait;

class Currency extends Model
{
    use QueryTrait;

    protected $table = 'currencies';

    const CODE_RUR = 'rur';

    public static $minUnitInCurrency = [
        self::CODE_RUR => 100
    ];
}
