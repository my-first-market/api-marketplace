<?php


namespace App\Models;


use App\Traits\QueryTrait;
use App\Traits\UserTrait;

class Lead extends Model
{
    use UserTrait, QueryTrait;
}
