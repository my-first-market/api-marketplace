<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Model extends BaseModel
{
    use SoftDeletes;

    protected $guarded = [
        'id'
    ];

    /**
     * The attributes for soft deleted
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
