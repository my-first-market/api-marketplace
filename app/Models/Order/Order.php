<?php


namespace App\Models\Order;


use App\Models\Model;
use App\Traits\CurrencyTrait;
use App\Traits\InfoContact;
use App\Traits\OrderProductTrait;
use App\Traits\QueryTrait;
use App\Traits\UserTrait;

class Order extends Model
{
    use CurrencyTrait, UserTrait, QueryTrait, OrderProductTrait, InfoContact;

    protected $guarded = [
    ];

    protected $fillable = ['id', 'status', 'shop_id', 'user_id', 'total_price', 'delivery_price', 'currency_id'];

    public $incrementing = false;

    const DEFAULT_INCREMENT = 1881919480;
    const MIN_RAND_INCREMENT = 10;
    const MAX_RAND_INCREMENT = 1844;

    /**
     * Создание id защищенных от брутфорса
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     *
     * @throws \Exception
     */
    public static function create(array $attributes = [])
    {
        $lastOrderId = (Order::latest('id')->first())->id ?? self::DEFAULT_INCREMENT;
        $lastOrderId = $lastOrderId < self::DEFAULT_INCREMENT ? self::DEFAULT_INCREMENT : $lastOrderId;
        $attributes['id'] = $lastOrderId + random_int(self::MIN_RAND_INCREMENT, self::MAX_RAND_INCREMENT);

        return (new static)->newQuery()->create($attributes);
    }
}
