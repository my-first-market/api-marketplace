<?php


namespace App\Models\Order;


use App\Models\Model;
use App\Traits\CurrencyTrait;
use App\Traits\ProductTrait;

class OrderProduct extends Model
{
    use CurrencyTrait, ProductTrait;
}
