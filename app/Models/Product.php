<?php


namespace App\Models;

use App\Traits\CurrencyTrait;
use App\Traits\ImageTrait;
use App\Traits\QueryTrait;
use App\Traits\ReviewTrait;
use App\Traits\ShopTrait;
use App\Traits\UserTrait;

class Product extends Model
{
    use UserTrait, QueryTrait, ShopTrait, CurrencyTrait, ImageTrait, ReviewTrait;
}
