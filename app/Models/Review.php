<?php


namespace App\Models;


use App\Traits\QueryTrait;
use App\Traits\ReviewTrait;
use App\Traits\UserTrait;

class Review extends Model
{
    use UserTrait, QueryTrait;
}
