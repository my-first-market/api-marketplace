<?php


namespace App\Models;


class Role extends Model
{
    public const ROLE_USER   = 'user';
    public const ROLE_ADMIN  = 'admin';
    public const ROLE_SELLER = 'seller';
}
