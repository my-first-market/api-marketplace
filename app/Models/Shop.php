<?php


namespace App\Models;


use App\Traits\ImageTrait;
use App\Traits\InfoContact;
use App\Traits\ProductTrait;
use App\Traits\QueryTrait;
use App\Traits\UserTrait;

class Shop extends Model
{
    use UserTrait, QueryTrait, ImageTrait, ProductTrait, InfoContact;

    protected $guarded = [
        'id'
    ];
}
