<?php

namespace App\Models;

use App\Traits\QueryTrait;
use App\Traits\ImageTrait;
use App\Traits\UserRoleTrait;
use \App\Traits\InfoContact;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasApiTokens, Notifiable, SoftDeletes, QueryTrait, ImageTrait, UserRoleTrait, InfoContact;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes for soft deleted
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
