<?php

namespace App\Providers;

use App\Events\SendEmailEvent;
use App\Events\UpdateOrCreateUserEvent;
use App\Listeners\SendEmailListener;
use App\Listeners\UpdateOrCreateUserListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UpdateOrCreateUserEvent::class => [
            UpdateOrCreateUserListener::class
        ],
        SendEmailEvent::class => [
            SendEmailListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
