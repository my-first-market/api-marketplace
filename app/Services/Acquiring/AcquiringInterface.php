<?php


namespace App\Services\Acquiring;


use App\Models\Acquiring\Acquiring;
use App\Models\Order;
use Illuminate\Http\Request;

interface AcquiringInterface
{
    /**
     * Метода создания заявки
     *
     * @param Acquiring $acquiring
     * @param $totalPrice
     * @return mixed
     */
    public function create(Acquiring $acquiring, $totalPrice);

    /**
     * Методо при успешной оплате
     *
     * @param Request $request
     * @return mixed
     */
    public static function success(Request $request);

    /**
     * Метод при неудачной оплате
     *
     * @param Request $request
     * @return mixed
     */
    public static function failed(Request $request);
}
