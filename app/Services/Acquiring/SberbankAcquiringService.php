<?php


namespace App\Services\Acquiring;


use App\DataModels\Status;
use App\Events\SendEmailEvent;
use App\Extensions\SberbankAcquiring\Client;
use App\Extensions\SberbankAcquiring\Currency;
use App\Helpers\CurrencyHelper;
use App\Models\Acquiring\Acquiring;
use App\Models\Acquiring\AcquiringOrder;
use App\Models\Acquiring\AcquiringTransaction;
use App\Models\Currency as CurrencyModel;
use App\Models\Order\Order;
use App\Models\User;
use App\Services\Mail\AcquiringMailService;
use Illuminate\Http\Request;

/**
 * Создание и обработка платежей в системе сбербанк
 *
 * Class SberbankAcquiringService
 * @package App\Services\Acquiring
 */
class SberbankAcquiringService implements AcquiringInterface
{
    protected $token;
    protected $api;
    protected $currency;

    public function __construct()
    {
        $this->token    = config('acquiring.sbr.token');
        $this->api      = config('acquiring.sbr.api');
        $this->currency = Currency::RUB;
    }

    /**
     * Create acquiring for Sberbank
     *
     * @param Acquiring $acquiring
     * @param $totalPrice
     * @return mixed
     *
     * @throws \App\Extensions\SberbankAcquiring\Exception\NetworkException
     */
    public function create(Acquiring $acquiring, $totalPrice)
    {
        $client = new Client([
            'apiUri' => $this->api,
            'token'  => $this->token
        ]);

        $acquiringTransaction = AcquiringTransaction::create([
            'status'       => Status::CODE_DRAFT,
            'acquiring_id' => $acquiring->id,
        ]);

        $amount = CurrencyHelper::minUnitInCurrency($totalPrice, CurrencyModel::CODE_RUR);

        if ($amount <= 0) throw new \Exception('Видимо кончился товар у продавца или стоимость товара равна нулю');

        $result = $client->execute('/payment/rest/register.do', [
            'orderNumber' => $acquiring->id,
            'amount'      => $amount,
            'language'    => 'ru',
            'returnUrl'   => route('acquiring.success'),
            'failUrl'     => route('acquiring.fail')
        ]);

        $acquiringTransaction->update([
            'status'             => Status::CODE_PENDING,
            'acquiring_order_id' => $result['orderId']
        ]);

        return $result['formUrl'];
    }

    /**
     * Если оплата прошла успешно, то изменяем все статусы и уменьшаем количество товара
     *
     * @param Request $request
     *
     * @return mixed
     */
    public static function success(Request $request)
    {
        $acquiringId = $request['orderId'];

        $acquiringTransaction = AcquiringTransaction::where('status', Status::CODE_PENDING)
            ->where('acquiring_order_id', $acquiringId)
            ->first();

        $acquiringTransaction->update([
            'status' => Status::CODE_PAID
        ]);

        $orderIds = AcquiringOrder::where('acquiring_id', $acquiringTransaction->acquiring_id)
            ->pluck('order_id')
            ->toArray();

        $orders = collect();
        $userId = 0;
        foreach ($orderIds as $orderId) {
            $order = Order::where('status', Status::CODE_PENDING)
                ->where('id', $orderId)
                ->first();

            $order->update([
                'status' => Status::CODE_PAID
            ]);

            $orders->push($order);
            $userId = $order->user_id;
            self::reduceAmountInProducts($order);
        }

        if ($user = User::find($userId)) {
            event(new SendEmailEvent(new AcquiringMailService($orders), $user));
        }

        return 'ok';
    }

    /**
     * Обновление статусов при неудачной оплате
     *
     * @param Request $request
     *
     * @return mixed
     */
    public static function failed(Request $request)
    {
        $acquiringId = $request['orderId'];

        $acquiringTransaction = AcquiringTransaction::where('status', Status::CODE_PENDING)
            ->where('acquiring_order_id', $acquiringId)
            ->first();

        $acquiringTransaction->update([
            'status' => Status::CODE_ERROR
        ]);

        $orderIds = AcquiringOrder::where('acquiring_id', $acquiringTransaction->acquiring_id)
            ->pluck('order_id')
            ->toArray();

        foreach ($orderIds as $orderId) {
            $order = Order::where('status', Status::CODE_PENDING)
                ->where('id', $orderId)
                ->first();

            $order->update([
                'status' => Status::CODE_ERROR
            ]);
        }

        return 'error';
    }

    /**
     * При оплате уменшаем количество заказанного товара у производителя
     *
     * @param Order $order
     */
    private static function reduceAmountInProducts(Order $order)
    {
        $orderProducts = $order->products;

        foreach ($orderProducts as $orderProduct) {
            $product = $orderProduct->product;

            $amount = $product->amount - $orderProduct->current_amount > 0 ? $product->amount - $orderProduct->current_amount : 0;
                // TODO создавать event c уведомление что товар был оплачен, но количество товара уже равно нулю
                $product->update([
                'amount' => $amount
            ]);
        }
    }
}
