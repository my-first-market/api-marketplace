<?php


namespace App\Services\Client;


use App\DataModels\Status;
use App\Helpers\ModelHelper;
use App\Models\Acquiring\Acquiring;
use App\Models\Acquiring\AcquiringOrder;
use App\Models\Currency;
use App\Models\InfoContact;
use App\Models\Order\Order;
use App\Models\Product;
use App\Services\Acquiring\SberbankAcquiringService;
use App\Services\Update\OrderProductUpdateService as OrderProductService;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Self_;

class CreateOrder
{
    const DEFAULT_DELIVERY = 0;

    /**
     * Метод для создания ордеров на клиенте
     *
     * @param Request $request
     *
     * @return CreateOrderDTO
     *
     * @throws \App\Extensions\SberbankAcquiring\Exception\NetworkException
     */
    public static function store(Request $request)
    {
        $requestProducts = $request['products'];
        $shopProducts    = [];

        foreach ($requestProducts as $item) {
            $product = Product::find($item['id']);
            $shopProducts[$product->shop_id][] = $item;
        }


        $orders = [];
        $totalPrice = 0.0;
        foreach ($shopProducts as $shopId => $products) {

            // создание ордера
            $order = Order::create([
                'status'         => Status::CODE_ACTIVE,
                'user_id'        => $request['user']['id'],
                'shop_id'        => $shopId,
                'currency_id'    => (Currency::where('code', 'rur')->first())->id ?? 1,
                'total_price'    => 0,
                'delivery_price' => empty($request['delivery_price']) ? self::DEFAULT_DELIVERY : $request['delivery_price'],
            ]);

            // добавление адреса доставки
            if (
                !empty($request['phone_mobile']) &&
                !empty($request['city']) &&
                !empty($request['address'])) {
                InfoContact::create([
                    'section'      => ModelHelper::SECTION_ORDER,
                    'item_id'      => $order->id,
                    'phone_mobile' => $request['phone_mobile'],
                    'country'      => $request['country'] ?? "Россия",
                    'city'         => $request['city'],
                    'address'      => $request['address']
                ]);
            }

            $request['products'] = $products;
            $order = (new OrderProductService($request, $order))->update();
            $orders[] = $order;
            $totalPrice += $order->total_price + $order->delivery_price;
        }

        // создание транзакции на оплату товаров
        // $acquiring = Acquiring::create([]);
        // $acquiringUrl = (new SberbankAcquiringService())->create($acquiring, $totalPrice);

        // foreach ($orders as $order) {
        //     AcquiringOrder::create([
        //         'acquiring_id' => $acquiring->id,
        //         'order_id'     => $order->id
        //     ]);

        //     $order->update([
        //         'status' => Status::CODE_PENDING
        //     ]);
        // }
        $acquiringUrl = 'OKKKK';
        return new CreateOrderDTO($orders, $acquiringUrl);
    }
}
