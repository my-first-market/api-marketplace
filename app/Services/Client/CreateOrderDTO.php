<?php


namespace App\Services\Client;


class CreateOrderDTO
{
    private $orders,
            $acquiringUrl;

    /**
     * CreateOrderDTO constructor.
     * @param $orders
     * @param $acquiringUrl
     */
    public function __construct($orders, $acquiringUrl)
    {
        $this->orders       = $orders;
        $this->acquiringUrl = $acquiringUrl;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function getUrl()
    {
        return $this->acquiringUrl;
    }
}
