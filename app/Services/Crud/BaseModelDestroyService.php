<?php


namespace App\Services\Crud;


use App\Models\Lead;
use App\Models\Order\Order;
use App\Models\Review;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

abstract class BaseModelDestroyService
{
    protected $modelClass;

    protected $notFilteredModels = [
        Order::class
    ];

    protected $notDeletedModels = [
        Order::class,
        User::class,
        Lead::class,
        Review::class
    ];

    public function destroy(int $id)
    {
        if (!$this->isAllowed()) {
            return null;
        }

        $this->before();

        $query = $this->modelClass::where('id', $id);
        $query = $this->queryBuilder($query);

        $model = $query->first();
        $model->delete();

        $this->before();

    }

    public function before()
    {

    }

    public function after()
    {

    }

    /**
     * @param $query
     * @return mixed
     */
    public function queryBuilder($query)
    {
        $user = Auth::user();

        if ($user->isSeller() && !in_array($this->modelClass, $this->notFilteredModels)) {
            $query = $query->where('user_id', $user->id);
        }

        return $query;
    }

    /**
     * @return bool
     */
    public function isAllowed()
    {
        $user = Auth::user();

        if (
            in_array($this->modelClass, $this->notDeletedModels) &&
            $user->isSeller() &&
            !$user->isAdmin()
        ) {
            return false;
        }

        return $user->isAdmin();
    }
}
