<?php


namespace App\Services\Crud;


use App\DataModels\QuerySort;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

abstract class BaseModelIndexService
{
    protected $modelClass;
    protected $paginate;
    protected $countRows;

    const DEFAULT_PER_PAGE = 1000;

    /**
     * Abstract method
     *
     * @param $models
     * @return mixed
     */
    abstract function resourceWrapper($models);

    public function init(Request $request)
    {
        $modelQuery = $this->modelClass::querySort(new QuerySort($request->all()));
        $queryCount = $this->modelClass::querySort(new QuerySort($request->all()));

        $resultQuery = $this->queryBuilder($modelQuery, $queryCount);

        $modelQuery = $resultQuery['query'];
        $queryCount = $resultQuery['queryCount'];

        $this->countRows = $queryCount->count();
        $perPage         = $request->perPage ?? self::DEFAULT_PER_PAGE;
        $page            = $request->page ?? 1;
        $this->paginate  = $modelQuery->paginate($perPage, ['*'], '', $page);
    }

    public function getResult()
    {
        $models = collect($this->paginate->items());

        return $this->resourceWrapper($models);
    }

    public function getMeta()
    {
        return (object)[
            'page'     => (int)$this->paginate->currentPage(),
            'perPage'  => (int)$this->paginate->perPage(),
            'total'    => (int)$this->countRows
        ];
    }

    /**
     * @param $query
     * @param $queryCount
     * @return mixed
     */
    public function queryBuilder($query, $queryCount)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return [
                'query'      => $query,
                'queryCount' => $queryCount
            ];
        }

        if ($user->isSeller()) {
            $query = $query->where('user_id', $user->id);
            $queryCount = $query->where('user_id', $user->id);
        }

        return [
            'query'      => $query,
            'queryCount' => $queryCount
        ];
    }
}
