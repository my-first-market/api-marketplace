<?php


namespace App\Services\Crud;


use App\Models\Order\Order;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

abstract class BaseModelShowService
{
    protected $modelClass;

    protected $notFilteredModels = [
        Order::class,
        User::class
    ];

    abstract function resourceWrapper($model);

    public function getResult(int $id)
    {
        $query = $this->modelClass::where('id', $id);
        $query = $this->queryBuilder($query);
        $model = $query->first();

        if (is_null($model)) throw new \Exception('Row not found');

        return $this->resourceWrapper($model);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function queryBuilder($query)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return $query;
        }

        if ($user->isSeller()) {
            $query = $query->where('user_id', $user->id);
        }

        return $query;
    }
}
