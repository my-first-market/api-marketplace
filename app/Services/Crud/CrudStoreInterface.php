<?php


namespace App\Services\Crud;


use Illuminate\Http\Request;

interface CrudStoreInterface
{
    public function getResult(Request $request);
}
