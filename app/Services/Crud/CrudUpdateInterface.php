<?php


namespace App\Services\Crud;


use Illuminate\Http\Request;

interface CrudUpdateInterface
{
    public function getResult(Request $request, $id);
}
