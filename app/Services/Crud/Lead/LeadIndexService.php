<?php


namespace App\Services\Crud\Lead;


use App\Http\Resources\LeadResource;
use App\Models\Lead;
use App\Services\Crud\BaseModelIndexService;
use Illuminate\Support\Facades\Auth;

class LeadIndexService extends BaseModelIndexService
{
    protected $modelClass = Lead::class;

    /**
     * @inheritDoc
     */
    function resourceWrapper($models)
    {
        return LeadResource::collection($models);
    }

    /**
     * @param $query
     * @param $queryCount
     * @return array|mixed
     * @throws \Exception
     */
    public function queryBuilder($query, $queryCount)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return [
                'query'      => $query,
                'queryCount' => $queryCount
            ];
        }

        throw new \Exception('permission denied');
    }
}
