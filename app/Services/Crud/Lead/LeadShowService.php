<?php


namespace App\Services\Crud\Lead;


use App\Http\Resources\LeadResource;
use App\Models\Lead;
use App\Services\Crud\BaseModelShowService;
use Illuminate\Support\Facades\Auth;

class LeadShowService extends BaseModelShowService
{

    protected $modelClass = Lead::class;

    function resourceWrapper($model)
    {
        return new LeadResource($model);
    }

    /**
     * @param $query
     * @return mixed
     * @throws \Exception
     */
    public function queryBuilder($query)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return $query;
        }

        throw new \Exception('permission denied');
    }
}
