<?php


namespace App\Services\Crud\Lead;


use App\DataModels\Status;
use App\Http\Resources\LeadResource;
use App\Models\Lead;
use App\Models\User;
use App\Services\Crud\CrudUpdateInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LeadUpdateService implements CrudUpdateInterface
{

    public function getResult(Request $request, $id)
    {
        if (!(Auth::user())->isAdmin()) {
            return 0;
        }

        $lead = Lead::find($id);

        $lead->update([
            'active' => $request['active']
        ]);

        $user = User::find($lead->user_id);
        $user->active = $request['active'];
        $user->save();

        if ($user->active) {
            $message = "<p>Теперь ваш аккаунт имеет статус продавца.</p><p><a href='https://admin.myeden.xyz'>Перейдите в админ. панель.</a></p>";
            \Mail::to($user->email)->send(new \App\Mail\RegisterMail('Поздравляем!', $message, 'Активация аккаунта продавца на myeden.xyz'));
        }

        return new LeadResource($lead);
    }
}
