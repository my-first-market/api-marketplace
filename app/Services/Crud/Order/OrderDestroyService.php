<?php


namespace App\Services\Crud\Order;


use App\Models\Order\Order;
use App\Services\Crud\BaseModelDestroyService;

class OrderDestroyService extends BaseModelDestroyService
{
    protected $modelClass = Order::class;
}
