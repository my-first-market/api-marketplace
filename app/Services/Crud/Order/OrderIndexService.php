<?php


namespace App\Services\Crud\Order;


use App\Http\Resources\Order as OrderResource;
use App\Models\Order\Order;
use App\Models\Shop;
use App\Services\Crud\BaseModelIndexService;
use Illuminate\Support\Facades\Auth;

class OrderIndexService extends BaseModelIndexService
{
    protected $modelClass = Order::class;

    public function resourceWrapper($models)
    {
        return OrderResource::collection($models);
    }

    public function queryBuilder($query, $queryCount)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return [
                'query'      => $query,
                'queryCount' => $queryCount
            ];
        }

        $shops = Shop::where('user_id', $user->id)->get();

        $shopIds = $shops->map(function ($item){
            return $item->id;
        })->toArray();

        $query = $query->whereIn('shop_id', $shopIds);
        $queryCount = $query->whereIn('shop_id', $shopIds);

        return [
            'query'      => $query,
            'queryCount' => $queryCount
        ];
    }
}
