<?php


namespace App\Services\Crud\Order;


use App\Http\Resources\Order as OrderResource;
use App\Models\Order\Order;
use App\Models\Shop;
use App\Services\Crud\BaseModelShowService;
use Illuminate\Support\Facades\Auth;

class OrderShowService extends BaseModelShowService
{
    protected $modelClass = Order::class;

    public function resourceWrapper($model)
    {
        return new OrderResource($model);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function queryBuilder($query)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return $query;
        }

        if ($user->isSeller()) {
            $shops = Shop::where('user_id', $user->id)->get();

            $shopIds = $shops->map(function ($item){
                return $item->id;
            })->toArray();

            $query = $query->whereIn('shop_id', $shopIds);
        }

        return $query;
    }
}
