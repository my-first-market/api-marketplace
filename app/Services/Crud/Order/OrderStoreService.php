<?php


namespace App\Services\Crud\Order;


use App\DataModels\Status;
use App\Http\Resources\Order as OrderResource;
use App\Models\Currency;
use App\Models\Order\Order;
use App\Services\Crud\CrudStoreInterface;
use App\Services\Update\OrderProductUpdateService as OrderProductService;
use Illuminate\Http\Request;

class OrderStoreService implements CrudStoreInterface
{
    public function getResult(Request $request)
    {
        $input = $request->all();

        $order = Order::create([
            'status' => Status::CODE_ACTIVE,
            'user_id' => $input['user']['id'],
            'currency_id' => (Currency::where('code', 'rur')->first())->id ?? 1,
            'total_price' => 0
        ]);

        (new OrderProductService($request, $order))->update();

        return new OrderResource($order);
    }
}
