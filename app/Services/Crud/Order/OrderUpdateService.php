<?php


namespace App\Services\Crud\Order;


use App\Http\Resources\Order as OrderResource;
use App\Models\Order\Order;
use App\Services\Crud\CrudUpdateInterface;
use Illuminate\Http\Request;

class OrderUpdateService implements CrudUpdateInterface
{
    public function getResult(Request $request, $id)
    {
        $order = Order::find($id);

        if (empty($order)) throw new \Exception('Order not found');

        if(isset($request['status'])) {
            $order->update([
                'status' => $request['status']
            ]);
        }

        return new OrderResource($order);
    }
}
