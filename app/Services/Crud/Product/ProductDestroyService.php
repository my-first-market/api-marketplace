<?php


namespace App\Services\Crud\Product;


use App\Models\Product;
use App\Services\Crud\BaseModelDestroyService;
use Illuminate\Support\Facades\Auth;

class ProductDestroyService extends BaseModelDestroyService
{
    protected $modelClass = Product::class;

    public function isAllowed()
    {
        $user = Auth::user();

        return $user->isSeller() || $user->isAdmin();
    }
}
