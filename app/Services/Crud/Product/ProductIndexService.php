<?php


namespace App\Services\Crud\Product;


use App\Models\Product;
use App\Http\Resources\Product as ProductResource;
use App\Services\Crud\BaseModelIndexService;
use Illuminate\Support\Facades\Auth;

class ProductIndexService extends BaseModelIndexService
{
    protected $modelClass = Product::class;

    public function resourceWrapper($models)
    {
        return ProductResource::collection($models);
    }
}
