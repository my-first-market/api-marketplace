<?php


namespace App\Services\Crud\Product;


use App\Http\Resources\Product as ProductResource;
use App\Models\Product;
use App\Services\Crud\BaseModelShowService;

class ProductShowService extends BaseModelShowService
{
    protected $modelClass = Product::class;

    public function resourceWrapper($model)
    {
        return new ProductResource($model);
    }
}
