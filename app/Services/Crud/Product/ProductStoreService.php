<?php


namespace App\Services\Crud\Product;


use App\Http\Resources\Product as ProductResource;
use App\Models\Product;
use App\Models\Shop;
use App\Services\Crud\CrudStoreInterface;
use App\Services\Update\ImageUpdateService as ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductStoreService implements CrudStoreInterface
{
    public function getResult(Request $request)
    {
        $input = $request->all();

        $product= Product::create([
            'name' => $input['name'],
            'active' => $input['active'] ?? false,
            'description' => $input['description'] ?? null,
            'html' => $input['html'] ?? null,
            'user_id' => (Shop::find($input['shop']['id']))->user_id,
            'price' => $input['price'] ?? 0,
            'amount' => $input['amount'] ?? 0,
            'currency_id' => $input['currency']['id'] ?? 1,
            'shop_id' => $input['shop']['id']
        ]);

        (new ImageService($request, $product))->update();

        return new ProductResource($product);
    }
}
