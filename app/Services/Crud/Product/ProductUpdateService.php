<?php


namespace App\Services\Crud\Product;


use App\Models\Product;
use App\Models\Shop;
use App\Http\Resources\Product as ProductResource;
use App\Services\Crud\CrudUpdateInterface;
use App\Services\Update\ImageUpdateService as ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductUpdateService implements CrudUpdateInterface
{
    public function getResult(Request $request, $id)
    {
        $query = Product::where('id', $id);
        $user = Auth::user();

        if ($user->isSeller()) {
            $query = $query->where('user_id', $user->id);
        }

        $product = $query->first();

        if (empty($product)) throw new \Exception('Product not found');

        $input = $request->all();

        $product->update([
            'name' => $input['name'],
            'active' => $input['active'],
            'description' => $input['description'] ?? null,
            'html' => $input['html'] ?? null,
            'user_id' => $input['user']['id'] ?? (Shop::find($input['shop']['id']))->user_id,
            'price' => $input['price'] ?? 0,
            'amount' => $input['amount'] ?? 0,
            'currency_id' => $input['currency']['id'] ?? 1,
            'shop_id' => $input['shop']['id'] ?? $product->shop_id
        ]);

        (new ImageService($request, $product))->update();

        return new ProductResource($product);
    }
}
