<?php


namespace App\Services\Crud\Review;


use App\Models\Product;
use App\Models\Review;
use App\Services\Crud\BaseModelDestroyService;

class ReviewDestroyService extends BaseModelDestroyService
{
    protected $modelClass = Review::class;
}
