<?php


namespace App\Services\Crud\Review;


use App\DataModels\Status;
use App\Helpers\ModelHelper;
use App\Http\Resources\ReviewResource;
use App\Models\Product;
use App\Models\Review;
use App\Services\Crud\BaseModelIndexService;
use Illuminate\Support\Facades\Auth;

class ReviewIndexService extends BaseModelIndexService
{
    protected $modelClass = Review::class;

    public function resourceWrapper($models)
    {
        return ReviewResource::collection($models);
    }

    /**
     * @param $query
     * @param $queryCount
     * @return mixed
     */
    public function queryBuilder($query, $queryCount)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return [
                'query'      => $query,
                'queryCount' => $queryCount
            ];
        }

        if ($user->isSeller()) {

            $productIds = Product::where('active', Status::CODE_TRUE)
                ->where('user_id', $user->id)->pluck('id')->toArray();

            $query = $query->whereIn('item_id', $productIds)->where('section', ModelHelper::SECTION_PRODUCT);
            $queryCount = $query->whereIn('item_id', $productIds)->where('section', ModelHelper::SECTION_PRODUCT);;
        }

        return [
            'query'      => $query,
            'queryCount' => $queryCount
        ];
    }
}
