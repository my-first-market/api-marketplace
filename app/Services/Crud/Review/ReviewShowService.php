<?php


namespace App\Services\Crud\Review;


use App\DataModels\Status;
use App\Helpers\ModelHelper;
use App\Http\Resources\ReviewResource;
use App\Models\Product;
use App\Models\Review;
use App\Services\Crud\BaseModelShowService;
use Illuminate\Support\Facades\Auth;

class ReviewShowService extends BaseModelShowService
{
    protected $modelClass = Review::class;

    public function resourceWrapper($model)
    {
        return new ReviewResource($model);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function queryBuilder($query)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return $query;
        }

        if ($user->isSeller()) {
            $productIds = Product::where('status', Status::CODE_ACTIVE)
                ->where('user_id', $user->id)->pluck('id')->toArray();

            $query = $query->whereIn('item_id', $productIds)->where('section', ModelHelper::SECTION_PRODUCT);
        }

        return $query;
    }
}
