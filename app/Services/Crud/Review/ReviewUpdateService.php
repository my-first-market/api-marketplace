<?php


namespace App\Services\Crud\Review;


use App\Http\Resources\ReviewResource;
use App\Models\Product;
use App\Models\Review;
use App\Models\Shop;
use App\Http\Resources\Product as ProductResource;
use App\Services\Crud\CrudUpdateInterface;
use App\Services\Update\ImageUpdateService as ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewUpdateService implements CrudUpdateInterface
{
    public function getResult(Request $request, $id)
    {
        $review = Review::where('id', $id)->first();

        $review->update([
            'active'  => $request['active'],
            'comment' => $request['comment']
        ]);

        return new ReviewResource($review);
    }
}
