<?php


namespace App\Services\Crud\Shop;


use App\Models\Shop;
use App\Services\Crud\BaseModelDestroyService;
use Illuminate\Support\Facades\Auth;

class ShopDestroyService extends BaseModelDestroyService
{
    protected $modelClass = Shop::class;

    public function isAllowed()
    {
        $user = Auth::user();

        return $user->isSeller() || $user->isAdmin();
    }

    //TODO реализовать удалление связанных продуктов
}
