<?php


namespace App\Services\Crud\Shop;

use App\Http\Resources\Shop as ShopResource;
use App\Models\Shop;
use App\Services\Crud\BaseModelIndexService;
use Illuminate\Support\Facades\Auth;

class ShopIndexService extends BaseModelIndexService
{
    protected $modelClass = Shop::class;

    public function resourceWrapper($models)
    {
        return ShopResource::collection($models);
    }
}
