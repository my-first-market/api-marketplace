<?php


namespace App\Services\Crud\Shop;


use App\Http\Resources\Shop as ShopResource;
use App\Models\Shop;
use App\Services\Crud\BaseModelShowService;
use Illuminate\Support\Facades\Auth;

class ShopShowService extends BaseModelShowService
{
    protected $modelClass = Shop::class;

    public function resourceWrapper($model)
    {
        return new ShopResource($model);
    }
}
