<?php


namespace App\Services\Crud\Shop;


use App\Models\Shop;
use App\Http\Resources\Shop as ShopResource;
use App\Services\Crud\CrudStoreInterface;
use App\Services\Update\ImageUpdateService as ImageService;
use App\Services\Update\InfoContactService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopStoreService implements CrudStoreInterface
{
    public function getResult(Request $request)
    {
        $input = $request->all();

        //TODO добавить валидатор

        $shop = Shop::create([
            'name' => $input['name'],
            'active' => $input['active'] ?? false,
            'description' => $input['description'] ?? null,
            'html' => $input['html'] ?? null,
            'user_id' => $input['user']['id'] ?? Auth::id(),
        ]);

        (new ImageService($request, $shop))->update();
        (new InfoContactService($request, $shop))->update();

        return new ShopResource($shop);
    }
}
