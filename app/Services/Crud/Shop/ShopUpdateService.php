<?php


namespace App\Services\Crud\Shop;


use App\Models\Shop;
use App\Http\Resources\Shop as ShopResource;
use App\Services\Crud\BaseModelUpdateService;
use App\Services\Crud\CrudUpdateInterface;
use App\Services\Update\ImageUpdateService as ImageService;
use App\Services\Update\InfoContactService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopUpdateService implements CrudUpdateInterface
{
    public function getResult(Request $request, $id)
    {
        $query = Shop::where('id', $id);
        $user = Auth::user();

        if ($user->isSeller()) {
            $query = $query->where('user_id', $user->id);
        }

        $shop = $query->first();

        if (empty($shop)) throw new \Exception('Shop not found');

        $input = $request->all();

//        $validator = Validator::make($request->all(), [
//            'name' => 'unique:users,name,$this->id,id',
//            'email' => 'unique:users,email,$this->id,id'
//        ]);
//
//        if ($validator->fails()) return $this->sendFailsValidator($validator->errors());


        $shop->update([
            'name' => $input['name'],
            'active' => $input['active'] ?? false,
            'description' => $input['description'] ?? null,
            'html' => $input['html'] ?? null,
            'user_id' => $input['user']['id'] ?? $shop->user_id
        ]);

        (new ImageService($request, $shop))->update();
        (new InfoContactService($request, $shop))->update();

        return new ShopResource($shop);
    }
}
