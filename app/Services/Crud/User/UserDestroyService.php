<?php


namespace App\Services\Crud\User;


use App\Http\Resources\User\User as UserResource;
use App\Models\User;
use App\Services\Crud\BaseModelDestroyService;

class UserDestroyService extends BaseModelDestroyService
{
    protected $modelClass = User::class;
}
