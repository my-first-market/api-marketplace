<?php


namespace App\Services\Crud\User;


use App\Models\User;
use App\Http\Resources\User\User as UserResource;
use App\Services\Crud\BaseModelIndexService;
use Illuminate\Support\Facades\Auth;

class UserIndexService extends BaseModelIndexService
{
    protected $modelClass = User::class;

    public function resourceWrapper($models)
    {
        return UserResource::collection($models);
    }

    public function queryBuilder($query, $queryCount)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return [
                'query'      => $query,
                'queryCount' => $queryCount
            ];
        }

        $query = $query->where('id', $user->id);
        $queryCount = $query->where('id', $user->id);

        return [
            'query'      => $query,
            'queryCount' => $queryCount
        ];
    }
}
