<?php


namespace App\Services\Crud\User;


use App\Http\Resources\User\User as UserResource;
use App\Models\User;
use App\Services\Crud\BaseModelShowService;
use Illuminate\Support\Facades\Auth;

class UserShowService extends BaseModelShowService
{
    protected $modelClass = User::class;

    public function resourceWrapper($model)
    {
        return new UserResource($model);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function queryBuilder($query)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return $query;
        }

        if ($user->isSeller()) {
            $query = $query->where('id', $user->id);
        }

        return $query;
    }
}
