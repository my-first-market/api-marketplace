<?php


namespace App\Services\Crud\User;


use App\Events\UpdateOrCreateUserEvent;
use App\Http\Resources\User\User as UserResource;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use App\Services\Crud\CrudStoreInterface;
use App\Services\Update\ImageUpdateService as ImageService;
use App\Services\Update\InfoContactService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserStoreService implements CrudStoreInterface
{
    public function getResult(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required'
        ]);

        if ($validator->fails()) return $this->sendFailsValidator($validator->errors());

        $input['password'] = bcrypt($input['password']);
        $user = User::create([
            'active'    => $input['active'],
            'name'      => $input['name'],
            'email'     => $input['email'],
            'password'  => $input['password']
        ]);

        $role = Role::where('code', $input['role'] ?? Role::ROLE_USER)->first();

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        event(new UpdateOrCreateUserEvent($request, $user));

        return new UserResource($user);
    }
}
