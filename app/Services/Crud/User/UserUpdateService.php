<?php


namespace App\Services\Crud\User;


use App\Events\UpdateOrCreateUserEvent;
use App\Models\Role;
use App\Models\User;
use App\Http\Resources\User\User as UserResource;
use App\Models\UserRole;
use App\Services\Crud\CrudUpdateInterface;
use Illuminate\Http\Request;

class UserUpdateService implements CrudUpdateInterface
{
    protected $userFields = [
        'active',
        'name',
        'email',
    ];

    /**
     * Обновляем пользователя и возвращем его
     *
     * @param Request $request
     * @param $id
     *
     * @return UserResource
     *
     * @throws \Exception
     */
    public function getResult(Request $request, $id)
    {
        $user = User::find($id);

        if (empty($user)) throw new \Exception('User not found');

        $user->update($this->getUserFields($request));

        $this->updateOrCreateRole($request, $user);

        event(new UpdateOrCreateUserEvent($request, $user));

        return new UserResource($user);
    }

    /**
     * Получаем поля для одновления пользователя
     *
     * @param Request $request
     *
     * @return array
     */
    protected function getUserFields(Request $request)
    {
        $updateUserFields = [];

        if (!empty($request['password'])) {
            $updateUserFields['password'] = bcrypt($request['password']);
        }

        foreach ($this->userFields as $key) {
            if (isset($request[$key])) {
                $updateUserFields[$key] = $key === 'active' ? ($request[$key] == "true" ? true : false) : $request[$key];
            }
        }

        return $updateUserFields;
    }

    /**
     * Обновление или создание роли
     *
     * @param Request $request
     * @param User $user
     */
    protected function updateOrCreateRole(Request $request, User $user)
    {
        $input    = $request->all();

        if (empty($input['role'])) return;

        $role     = Role::where('code', $input['role'])->first();
        $userRole =  UserRole::where('user_id', $user->id)->where('role_id', $role->id)->first();

        UserRole::where('user_id', $user->id)->where('role_id', '<>', $role->id)->delete();

        if (empty($userRole)) {
            UserRole::create([
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);
        } else {
            $userRole->role_id = $role->id;
            $userRole->save();
        }
    }
}
