<?php


namespace App\Services\FileSystem;


use App\DataModels\Image as SrcImage;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Image
{
    /**
     * Upload images
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    public static function upload(Request $request)
    {
        $files = $request->file('images');
        $images = collect();
        foreach ($files as $file) {
            $imageSrc = new SrcImage($file->store(self::getImagePrefix(), 'public'));
            $images->push($imageSrc);
        }

        return $images;
    }

    /**
     * Get image file prefix
     *
     * @return string
     */
    public static function getImagePrefix()
    {
        $carbon = Carbon::now();
        $year = $carbon->year;
        $month = $carbon->monthName;
        return 'images' . DIRECTORY_SEPARATOR .
            'uploads' . DIRECTORY_SEPARATOR .
            $year . DIRECTORY_SEPARATOR .
            $month;
    }
}
