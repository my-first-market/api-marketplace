<?php


namespace App\Services\Import;


use App\Imports\ProductsImport;
use App\Models\Currency;
use App\Models\Image;
use App\Models\Product;
use Carbon\Carbon;
use Chumper\Zipper\Zipper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class ImportProductService
{
    public static function import(Request $request)
    {
        self::importImages($request);
    }

    public static function importImages(Request $request)
    {
//        $zipImages = $request->file('images');
//        //dd($request->all());
//        $fileZip   = $zipImages->store('import' . DIRECTORY_SEPARATOR . 'zip');

        $zipper    = new Zipper;

        // Извлекаем изображения из zip
        $zipper->make(storage_path('app' . DIRECTORY_SEPARATOR . $request['path_images']))
            ->extractTo(storage_path(self::importImagePath()));
        $zipper->close();

        $csvExport = Excel::toCollection(new ProductsImport(), storage_path('app' . DIRECTORY_SEPARATOR . $request['path_csv']));
        $imageFiles = File::files(storage_path(self::importImagePath()));

        $shop   = request('shop');
        $userId = Auth::id();

        foreach ($csvExport[0] as $row) {
            foreach ($imageFiles as $image) {
                if ($image->getFilename() == $row['image']) {
                    $product = Product::create([
                        'active'        => !empty($row['amount']) && $row['amount'] > 0 ? 1 : 0,
                        'name'          => $row['name'],
                        'description'   => $row['description'] ?? null,
                        'html'          => $row['html'] ?? null,
                        'user_id'       => $userId,
                        'amount'        => $row['amount'],
                        'price'         => $row['price'],
                        'shop_id'       => $shop['id'],
                        'currency_id'   => (Currency::where('code', Currency::CODE_RUR)->first())->id
                    ]);

                    $newFile = self::getImagePrefix() . DIRECTORY_SEPARATOR . Str::random(42) . '.' . $image->getExtension();
                    Storage::disk('public')->put($newFile, $image->getContents());

                    Image::create([
                        'model_class'   => get_class($product),
                        'item_id'       => $product->id,
                        'src'           => 'storage' . DIRECTORY_SEPARATOR . $newFile,
                        'type'          => 'logo'
                    ]);

                    Image::create([
                        'model_class'   => get_class($product),
                        'item_id'       => $product->id,
                        'src'           => 'storage' . DIRECTORY_SEPARATOR . $newFile,
                        'type'          => 'gallery'
                    ]);
                }
            }
        }
        File::deleteDirectory(storage_path(self::importPath()));
    }

    public static function csv(Request $request)
    {
        $zipImages = $request->file('csv');
        return $zipImages->storeAs('import' . DIRECTORY_SEPARATOR . 'csv', Str::random(42) . '.csv');
    }

    public static function images(Request $request)
    {
        $zipImages = $request->file('images');
        return $zipImages->store('import' . DIRECTORY_SEPARATOR . 'zip');
    }

    public static function importZipPath()
    {
        return 'app' . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . 'zip';
    }

    public static function importPath()
    {
        return 'app' . DIRECTORY_SEPARATOR . 'import';
    }

    public static function importImagePath()
    {
        return 'app' . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . 'images';
    }

    /**
     * Get image file prefix
     *
     * @return string
     */
    public static function getImagePrefix()
    {
        $carbon = Carbon::now();
        $year = $carbon->year;
        $month = $carbon->monthName;
        return 'images' . DIRECTORY_SEPARATOR .
            'uploads' . DIRECTORY_SEPARATOR .
            $year . DIRECTORY_SEPARATOR .
            $month;
    }

}
