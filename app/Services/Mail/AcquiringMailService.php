<?php


namespace App\Services\Mail;


use Illuminate\Support\Collection;

class AcquiringMailService extends SendMailService implements SendEmailInterface
{
    const MAIL_TEMPLATE = 'mail.mail-acquiring';

    private $orders;

    public function __construct(Collection $orders)
    {
        $this->subject   = 'Оплата на myeden.xyz';
        $this->preHeader = 'Подтверждение оплаты';
        $this->orders = $orders;
    }

    /**
     * Содержимое письом с информацие об оплаченных товарах
     *
     * @return string
     *
     * @throws \Throwable
     */
    protected function getHtml() : string
    {
        return view(self::MAIL_TEMPLATE, ['orders' => $this->orders])->render();
    }
}
