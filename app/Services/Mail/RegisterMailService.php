<?php


namespace App\Services\Mail;


class RegisterMailService extends SendMailService implements SendEmailInterface
{
    const MAIL_TEMPLATE = 'mail.mail-register';

    public function __construct()
    {
        $this->subject   = 'Регистрация на myeden.xyz';
        $this->preHeader = 'Поздравляем';
    }

    /**
     * Содержимое письма о регистрации
     *
     * @return string
     *
     * @throws \Throwable
     */
    protected function getHtml() : string
    {
        return view(self::MAIL_TEMPLATE)->render();
    }
}
