<?php


namespace App\Services\Mail;

interface SendEmailInterface
{
    /**
     * Метод отправки писем
     *
     * @param string $to
     * @return mixed
     */
    public function send(string $to);
}
