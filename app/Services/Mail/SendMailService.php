<?php


namespace App\Services\Mail;


abstract class SendMailService
{
    protected $preHeader;
    protected $subject;

    /**
     * Отправка писем
     *
     * @param string $to
     */
    public function send(string $to)
    {
        \Mail::to($to)->send(new \App\Mail\RegisterMail($this->preHeader, $this->getHtml(), $this->subject));
    }

    /**
     * Генерации html содержимого письма
     *
     * @return string
     */
    abstract protected function getHtml(): string;
}
