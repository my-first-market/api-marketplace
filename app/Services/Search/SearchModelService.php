<?php


namespace App\Services\Search;


use App\DataModels\SearchData;
use App\Helpers\ModelHelper;
use App\Http\Resources\Client\SearchResource;
use Illuminate\Http\Request;

class SearchModelService
{
    const COUNT_PAGINATE = 2;

    /**
     * Search like
     *
     * @param Request $request
     *
     * @return SearchData
     */
    public function getResult(Request $request) : SearchResource
    {
        $section = $request['section'];
        $modelClass = ModelHelper::getClass($section);

        if (!$modelClass) {
            return null;
        }

        $q = $request['q'];
        $page = $request['page'] ?? 1;

        $pagination = $modelClass::where('name', 'like', '%' . $q . '%')
            ->orWhere('description', 'like', '%' . $q . '%')
            ->paginate(self::COUNT_PAGINATE, ['*'], 'search', $page);

        $items= $pagination->map(function ($item){
            return (object)[
                'id'          => $item->id,
                'name'        => $item->name,
                'description' => $item->description
            ];
        });

        $searchResult = new SearchData([
            'items'       => $items,
            'lastPage'    => $pagination->lastPage(),
            'currentPage' => $pagination->currentPage()
        ]);

        return new SearchResource($searchResult);
    }
}
