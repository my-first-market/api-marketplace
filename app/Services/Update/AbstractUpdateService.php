<?php


namespace App\Services\Update;


use Illuminate\Http\Request;

/**
 * Абстрактный класс обновления
 *
 * Class AbstractUpdateService
 * @package App\Services\Update
 */
abstract class AbstractUpdateService
{
    /**
     * Модель сущности
     *
     * @var $model
     */
    protected $model;

    /**
     * Приходящий реквест
     *
     * @var Request $request
     */
    protected $request;

    public function __construct(Request $request, $model)
    {
        $this->model = $model;
        $this->request = $request;
    }

    abstract function update();
}
