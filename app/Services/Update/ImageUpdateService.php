<?php


namespace App\Services\Update;


use App\DataModels\Image as SrcImage;
use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Общий класс обновления картинок у сущности
 *
 * Class ImageUpdateService
 * @package App\Services\Update
 */
class ImageUpdateService extends AbstractUpdateService
{
    /**
     * Обновление картинок у сущности
     */
    public function update()
    {
        $model = $this->model;
        $request = $this->request;

        // TODO переделать на фабрику
        if (isset($request['logo'])) {
            $this->saveImage($request['logo'], $model, 'logo');
        }

        if (isset($request['gallery'])) {
            $this->saveImages($request['gallery'], $model, 'gallery');
        }
    }

    /**
     * Обновление одной картинки у модели
     *
     * @param $srcImage
     * @param $model
     * @param $type
     */
    private function saveImage($srcImage, $model, $type)
    {
        if (is_array($srcImage) && !empty($srcImage['src'])) {
            $srcImage = $srcImage['src'];
        }

        if (!empty($srcImage)) {
            $srcImage = str_replace(url('/') . '/', '', $srcImage);
            $srcImage = str_replace(url('/'), '', $srcImage);
            $image = $model->imageQuery($type)->first();

            if(!$image) {
                Image::create([
                    'model_class'   => get_class($model),
                    'item_id'       => $model->id,
                    'src'           => $srcImage,
                    'type'          => $type ?? ''
                ]);
            } else {
                $image->update([
                    'src' => $srcImage
                ]);
            }
        } else {
            $model->imageQuery($type)->delete();
        }
    }

    /**
     * Обновление картинок
     *
     * @param $requestImages
     * @param $model
     * @param $type
     */
    private function saveImages($requestImages, $model, $type)
    {
        if (!empty($requestImages)) {

            $requestImages = collect($requestImages);

            $mapImages = $requestImages->map(function ($item){
                $item = str_replace(url('/') . '/', '', $item);
                $item = str_replace(url('/'), '', $item);
                return isset($item['src']) ? $item['src'] : $item;
            })->toArray();

            // Удаляем то, что не пришло в обновлении
            Image::whereNotIn('src', $mapImages)
                ->where('item_id', $model->id)
                ->where('type', $type)
                ->delete();

            $dbImages         = Image::where('item_id')->where('type', $type)->pluck('src')->toArray();
            $diffImages       = $requestImages->diff($dbImages);

            foreach ($diffImages as $src) {
                Image::create([
                    'model_class'   => get_class($model),
                    'item_id'       => $model->id,
                    'src'           => $src,
                    'type'          => $type ?? ''
                ]);
            }
        } else {
            Image::where('item_id', $model->id)
                ->where('type', $type)
                ->delete();
        }
    }

    /**
     * Check exist images on model
     *
     * @param $model
     * @param $type
     *
     * @return bool
     */
    private function existImages($model, $type)
    {
        $images = $model->imageQuery($type)->get();

        if ($images->count() > 0) {
            return true;
        }

        return false;
    }
}
