<?php


namespace App\Services\Update;


use App\Models\InfoContact;

/**
 * Сервис для работы с контактами
 *
 * Class InfoContactService
 * @package App\Services\Update
 */
class InfoContactService extends AbstractUpdateService
{
    public function update()
    {
        // если данные не пустые и существуют контакты
        if ($this->existContact() && !$this->isEmptyRequest()) {
            $this->delete();
            return;
        }

        // если пустые данные ничего не делаем
        if ($this->isEmptyRequest()) return;

        // создаем или обновляем данные
        $this->updateOrCreate();
    }

    /**
     * Создать или обновить
     */
    private function updateOrCreate()
    {
        $modelTable     = $this->model->getTable();
        $itemId         = $this->model->id;
        $requestContact = $this->request['contact'];

        $requestFields = collect($requestContact)->filter(function ($value, $key){
            return !empty($value);
        })->toArray();

        if (empty($requestFields) && $this->existContact()) {
            $this->delete();
            return;
        }

        if (empty($requestFields)) {
            return;
        }

        if ($this->existContact()) {
            InfoContact::where('section', $modelTable)
                ->where('item_id', $itemId)
                ->update($requestContact);
        } else {
            $requestContact['section'] = $modelTable;
            $requestContact['item_id'] = $itemId;
            InfoContact::create($requestContact);
        }
    }

    /**
     * Удаляем контакт
     */
    private function delete()
    {
        $modelTable = $this->model->getTable();
        $itemId     = $this->model->id;

        InfoContact::where('section', $modelTable)
            ->where('item_id', $itemId)
            ->delete();
    }

    /**
     * Проверка на существование контакта
     *
     * @return bool
     */
    private function existContact()
    {
        $modelTable  = $this->model->getTable();
        $itemId      = $this->model->id;
        $infoContact = InfoContact::where('section', $modelTable)
            ->where('item_id', $itemId)
            ->first();

        if ($infoContact) return true;

        return false;
    }

    /**
     * Проверка на пустые параметры
     *
     * @return bool
     */
    private function isEmptyRequest()
    {
        $requestContact = $this->request['contact'];
        if (empty($requestContact)  && !is_array($requestContact)) return true;

        foreach ($this->request['contact'] as $item) {
            if (!empty($item)) return false;
        }

        return true;
    }
}
