<?php


namespace App\Services\Update;


use App\Models\Product;

class OrderProductUpdateService extends AbstractUpdateService
{
    /**
     * Обновляем количество продуктов в ордере
     */
    public function update()
    {
        $productsRequest = $this->request['products'];
        $order           = $this->model;

        $orderProductIds = collect($productsRequest)->map(function ($item){
            return $item['id'];
        })->toArray();

        $order->products()->whereNotIn('product_id', $orderProductIds)->delete();

        $totalPrice = 0.0;
        foreach ($productsRequest as $productRequest) {
            $product = Product::find($productRequest['id']);
            $orderProduct = $order->products()->where('product_id', $product->id)->first();

            if ($product->amount - $productRequest['amount'] < 0) {
                continue;
            }

            if ($orderProduct) {
                $orderProduct->update([
                    'current_weight' => $product->weight ?? null,
                    'current_amount' => $productRequest['amount'] ?? 0,
                    'description'     => $productRequest['description'] ?? ""
                ]);
            } else {
                $order->products()->create([
                    'order_id'       => $order->id,
                    'product_id'     => $product->id,
                    'current_weight' => $product->weight,
                    'current_amount' => $productRequest['amount'] ?? 1,
                    'current_price'  => $product->price ?? $productRequest->product,
                    'currency_id'    => $product->currency_id,
                    'description'     => $productRequest['description'] ?? ""
                ]);
            }


            $totalPrice += $product->price * $productRequest['amount'];
        }

        $order->update([
            'total_price' => $totalPrice
        ]);

        return $order;
    }
}
