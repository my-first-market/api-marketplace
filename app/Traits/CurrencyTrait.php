<?php


namespace App\Traits;


trait CurrencyTrait
{
    public function currency()
    {
        return $this->hasOne(\App\Models\Currency::class, 'id', 'currency_id');
    }
}
