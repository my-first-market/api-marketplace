<?php


namespace App\Traits;


use App\Http\Resources\Image;
use App\Models\Image as ImageModel;

trait ImageTrait
{
    public function image()
    {
        return $this->hasOne(ImageModel::class, 'item_id', 'id')
            ->where('model_class', get_class($this));
    }

    public function images()
    {
        return $this->hasMany(ImageModel::class, 'item_id', 'id')
            ->where('model_class', get_class($this));
    }

    /**
     * Get image Query for model
     *
     * @param null $type
     *
     * @return mixed
     */
    public function imageQuery($type = null)
    {
        $query = $this->images();

        if (!is_null($type)) {
            $query->where('type', $type);
        }

        return $query;
    }

    /**
     * Get SRC for first image by Model
     *
     * @param null $type
     *
     * @return string|null
     */
    public function getImageSrc($type = null)
    {
        $query = $this->image();

        if (!is_null($type)) {
            $query->where('type', $type);
        }

        $image = $query->first();

         return $image ? new Image($image) : null;
    }
}
