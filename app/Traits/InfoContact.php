<?php


namespace App\Traits;


trait InfoContact
{
    public function contact()
    {
        return $this->hasOne(\App\Models\InfoContact::class, 'item_id', 'id')
            ->where('section', $this->getTable());
    }
}
