<?php


namespace App\Traits;


use App\Models\Order\OrderProduct;

trait OrderProductTrait
{
    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }

    /**
     * Has products in order
     *
     * @return bool
     */
    public function hasProducts()
    {
        $productsCount = ($this->products)->count();

        if ($productsCount == 0) {
            return false;
        }

        return true;
    }
}
