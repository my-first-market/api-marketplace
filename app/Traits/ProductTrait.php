<?php


namespace App\Traits;


use App\Models\Product;

trait ProductTrait
{
    public function product()
    {
        return $this->hasOne(\App\Models\Product::class, 'id', 'product_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
