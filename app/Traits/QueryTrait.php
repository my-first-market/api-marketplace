<?php


namespace App\Traits;


use App\DataModels\QuerySort;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;

trait QueryTrait
{
    public static function querySort(QuerySort $dataQuerySort)
    {
        if (!empty($dataQuerySort->start) && !empty($dataQuerySort->end)) {
            $query = self::whereBetween('id', [$dataQuerySort->start, $dataQuerySort->end])
                ->orderBy($dataQuerySort->sort, $dataQuerySort->order);
        } else {
            $query = self::orderBy($dataQuerySort->sort, $dataQuerySort->order);
        }

        return $query;
    }

    public static function querySearch($query, Request $request)
    {
        if (!empty($request['searchQuery'])) {
            $query = $query->where('name', 'like', '%' . $request['searchQuery'] . '%')
                ->orWhere('description', 'like', '%' . $request['searchQuery'] . '%');
        }

        if (!empty($request['rating'])) {
            $query = $query->whereBetween('rating', [$request['rating'], $request['rating'] + 0.99]);
        }

        if (!empty($request['minPrice']) && !empty($request['maxPrice'])) {
            $query = $query->whereBetween('price', [$request['minPrice'], $request['maxPrice']]);
        }

        return $query;
    }
}
