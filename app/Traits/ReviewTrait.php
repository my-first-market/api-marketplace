<?php


namespace App\Traits;


use App\DataModels\Status;
use App\Models\Image as ImageModel;
use App\Models\Review;

trait ReviewTrait
{
    public function reviews()
    {
        return $this->hasMany(Review::class, 'item_id', 'id')
            ->where('active', Status::CODE_TRUE)
            ->where('section', $this->getTable())
            ->orderBy('created_at', 'DESC');
    }
}
