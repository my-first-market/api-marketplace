<?php


namespace App\Traits;


trait ShopTrait
{
    public function shop()
    {
        return $this->hasOne(\App\Models\Shop::class, 'id', 'shop_id');
    }
}
