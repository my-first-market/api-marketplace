<?php


namespace App\Traits;


use App\Models\Role;
use App\Models\User;

trait UserRoleTrait
{
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id')
            ->whereNull('user_roles.deleted_at');
    }

    public function isAdmin()
    {
        return in_array(Role::ROLE_ADMIN, $this->roles()->pluck('code')->toArray());
    }

    public function isSeller()
    {
        return in_array(Role::ROLE_SELLER, $this->roles()->pluck('code')->toArray());
    }
}
