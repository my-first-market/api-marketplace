<?php


namespace App\Traits;


trait UserTrait
{
    public function user()
    {
        return $this->hasOne(\App\Models\User::class, 'id', 'user_id');
    }
}
