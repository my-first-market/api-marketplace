<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sberbank Acquiring
    |--------------------------------------------------------------------------
    |
    */
    'sbr' => [
        'name'     => env('SBERBANK_NAME', ''),
        'password' => env('SBERBANK_PASSWORD', ''),
        'token'    => env('SBERBANK_TOKEN', ''),
        'api'      => env('SBERBANK_API', '')
    ],
    'redirect' =>[
        'success'  => env('ACQUIRING_URL_SUCCESS', ''),
        'fails'    => env('ACQUIRING_URL_FAILS', ''),
    ]
];
