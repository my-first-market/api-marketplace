<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnProductIdInOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('orders', 'product_id')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('product_id');
            });
        }

        if(Schema::hasColumn('orders', 'amount')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('amount');
            });
        }

        if(Schema::hasColumn('orders', 'price')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('price');
            });
        }

        if(Schema::hasColumn('orders', 'currency_id')) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('currency_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
