<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcquiringTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acquiring_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status');
            $table->integer('order_id');
            $table->string('order_code');                       // используется вв ссылке чтобы не понять, что оредера были отправлены
            $table->string('acquiring_order_id')->nullable();   // id платежа от платежной системы
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acquiring_transactions');
    }
}
