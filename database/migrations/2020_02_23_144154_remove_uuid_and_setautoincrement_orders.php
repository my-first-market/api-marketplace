<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RemoveUuidAndSetautoincrementOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('orders', 'uuid')) {
            Schema::table('orders', function (Blueprint $table){
                $table->dropColumn('uuid');
            });
        }

        DB::update("ALTER TABLE orders AUTO_INCREMENT = 1919480;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
