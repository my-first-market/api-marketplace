<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('section', 50);
            $table->index('section');
            $table->bigInteger('item_id');
            $table->index('item_id');
            $table->string('phone_mobile');
            $table->string('phone_home');
            $table->string('country');
            $table->index('country');
            $table->string('city');
            $table->index('city');
            $table->string('address');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info');
    }
}
