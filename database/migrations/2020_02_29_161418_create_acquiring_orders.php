<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcquiringOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // связь приобретения с заказом. Одно приобретения может иметь несколько заказов
        Schema::create('acquiring_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('acquiring_id');
            $table->bigInteger('order_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acquiring_orders');
    }
}
