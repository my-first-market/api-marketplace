<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAndInsercCollumnToAcquiringTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acquiring_transactions', function (Blueprint $table) {
            $table->bigInteger('acquiring_id')->after('status');
            $table->bigInteger('old_order_id')->after('acquiring_order_id');
        });

        $acquiringTransactions = DB::table('acquiring_transactions')->get();

        foreach ($acquiringTransactions as $transaction) {
            DB::table('acquiring_transactions')->where('id', $transaction->id)->update([
                'old_order_id' => $transaction->order_id
            ]);
        }

        if (Schema::hasColumn('acquiring_transactions', 'order_id')) {
            Schema::table('acquiring_transactions', function (Blueprint $table){
                $table->dropColumn('order_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acquiring_transactions', function (Blueprint $table) {
            //
        });
    }
}
