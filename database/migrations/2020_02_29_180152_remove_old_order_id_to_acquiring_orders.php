<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldOrderIdToAcquiringOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new ReinitAcquiringTransactions())->run();

        if (Schema::hasColumn('acquiring_transactions', 'old_order_id')) {
            Schema::table('acquiring_transactions', function (Blueprint $table){
                $table->dropColumn('old_order_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acquiring_orders', function (Blueprint $table) {
            //
        });
    }
}
