<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOrderCodeToAcquiringTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('acquiring_transactions', 'order_code')) {
            Schema::table('acquiring_transactions', function (Blueprint $table){
                $table->dropColumn('order_code');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acquiring_transaction', function (Blueprint $table) {
            //
        });
    }
}
