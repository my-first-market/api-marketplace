<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status', 10);
            $table->index('status');
            $table->bigInteger('user_id');
            $table->index('user_id');
            $table->string('section', 50);
            $table->index('section');
            $table->bigInteger('item_id');
            $table->index('item_id');
            $table->text('comment');
            $table->integer('rating');
            $table->index('rating');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
