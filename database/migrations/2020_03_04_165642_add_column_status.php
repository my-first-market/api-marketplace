<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table){
            $table->string('status', 10)
                ->after('id')
                ->default(\App\DataModels\Status::CODE_PENDING);
        });

        Schema::table('products', function (Blueprint $table){
            $table->string('status', 10)
                ->after('id')
                ->default(\App\DataModels\Status::CODE_PENDING);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
