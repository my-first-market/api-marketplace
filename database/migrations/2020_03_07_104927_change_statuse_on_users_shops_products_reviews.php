<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatuseOnUsersShopsProductsReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'reviews',
            'products',
            'shops',
            'users'
        ];

        foreach ($tables as $tableName) {
            if (Schema::hasColumn($tableName, 'status')) {
                Schema::table($tableName, function (Blueprint $table){
                    $table->renameColumn('status', 'old_status');
                });

                Schema::table($tableName, function (Blueprint $table){
                    $table->boolean('active')->default(false)->after('id');
                });

                DB::table($tableName)->orderBy('id')->chunk(10, function ($items) use ($tableName){
                    foreach ($items as $item) {
                        DB::table($tableName)
                            ->where('id', $item->id)
                            ->update([
                                'active' => $item->old_status == \App\DataModels\Status::CODE_ACTIVE
                            ]);
                    }
                });

                Schema::table($tableName, function (Blueprint $table){
                    $table->dropColumn('old_status');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
