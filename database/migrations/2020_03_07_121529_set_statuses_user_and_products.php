<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetStatusesUserAndProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'users',
            'products'
        ];

        foreach ($tables as $tableName) {
            DB::table($tableName)->orderBy('id')->chunk(10, function ($items) use ($tableName){
                foreach ($items as $item) {
                    DB::table($tableName)
                        ->where('id', $item->id)
                        ->update([
                            'active' => true
                        ]);
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
