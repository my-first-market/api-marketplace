<?php

use Illuminate\Database\Seeder;

class AddUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt(12345678);

        $users = [
            [
                'user' => [
                    'name' => 'admin',
                    'email' => 'admin@myeden.xyz',
                    'password' => $password,
                ],
                'role' => [
                    'code' => \App\Models\Role::ROLE_ADMIN,
                    'name' => 'Администратор'
                ]
            ],
            [
                'user' => [
                    'name' => 'seller',
                    'email' => 'seller@myeden.xyz',
                    'password' => $password,
                ],
                'role' => [
                    'code' => \App\Models\Role::ROLE_SELLER,
                    'name' => 'Продавец'
                ]
            ],
            [
                'user' => [
                    'name' => 'user',
                    'email' => 'user@myeden.xyz',
                    'password' => $password,
                ],
                'role' => [
                    'code' => \App\Models\Role::ROLE_USER,
                    'name' => 'Пользователь'
                ]
            ]
        ];

        foreach ($users as $item) {
            $userId = DB::table('users')->insertGetId($item['user']);
            $roleId = DB::table('roles')->insertGetId($item['role']);

            DB::table('user_roles')->insert([
                'user_id' => $userId,
                'role_id' => $roleId
            ]);
        }

        $userRoleId = DB::table('roles')->where('code', \App\Models\Role::ROLE_USER)->value('id');

        $userIds = DB::table('users')->pluck('id');

        foreach ($userIds as $userId) {
            DB::table('user_roles')->insert([
                'user_id' => $userId,
                'role_id' => $userRoleId
            ]);
        }
    }
}
