<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReinitAcquiringTransactions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $acquiringTransactions = DB::table('acquiring_transactions')->get();

        foreach ($acquiringTransactions as $transaction) {

            $acquiringId = DB::table('acquiring')->insertGetId([
                'id' => $transaction->old_order_id
            ]);
            DB::table('acquiring_transactions')
                ->where('id', $transaction->id)
                ->update([
                    'acquiring_id' => $acquiringId
                ]);

            DB::table('acquiring_orders')->insert([
                'acquiring_id' => $acquiringId,
                'order_id' => $transaction->old_order_id
            ]);
        }
    }
}
