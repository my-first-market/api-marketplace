@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Подтверждене Вашего Email адреса') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Этот Email адрес был подтвержден') }}
                        </div>
                    @endif

                    {{ __('Для подтверждения Email адреса, нажмите на кнопку.') }}
                    {{ __('Если вы не получили письмо') }}, <a href="{{ route('verification.resend') }}">{{ __('нажмите, чтобы получить другое.') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
