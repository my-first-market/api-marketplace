<p>Оплата на myeden.xyz</p>
<p>Заказ был успешно оплачен.</p>
@php
    $totalPrice = 0;
@endphp
@foreach($orders as $order)
    <p>
        Заказ: {{ $order->id }}<br>
        Стоимость: {{ $order->total_price }}<br>
        Доставка: {{ $order->delivery_price }}<br>
        <b>Итого:</b> {{ ($order->total_price + $order->delivery_price) }}
    </p>
    @php
        $totalPrice += $order->total_price + $order->delivery_price;
    @endphp
@endforeach
<p><b>Общая стоимость: {{ $totalPrice }}</b></p>
