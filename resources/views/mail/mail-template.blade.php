@extends('layouts.mail')

@section('preheader')
    {{ $preheader ?? "" }}
@endsection

@section('content')
    {!! $html !!}
@endsection
