<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*
|--------------------------------------------------------------------------
| ADMIN - CRUD
|--------------------------------------------------------------------------
|
| Проверка прав доступа. Пользователи с ролью user не имеют доступа
|
*/

Route::apiResource('shops', 'Api\Admin\ShopController');

Route::resource('products', 'Api\Admin\ProductController');

Route::resource('orders', 'Api\Admin\OrderController');

Route::resource('users', 'Api\Admin\UserController');

Route::resource('reviews', 'Api\Admin\ReviewController');

Route::resource('leads', 'Api\Admin\LeadController');

Route::prefix('import')->group(function (){
    Route::post('products', 'Api\Admin\ImportController@store');
    Route::post('csv', 'Api\Admin\ImportController@csv');
    Route::post('images', 'Api\Admin\ImportController@images');
});

Route::prefix('export')->group(function (){
    Route::get('orders', 'Api\Admin\ExportOrderController@xml');
});

/*
|--------------------------------------------------------------------------
| COMMON- CRUD
|--------------------------------------------------------------------------
|
| Общие методы используемы и в админке и на клиенте
|
*/

Route::resource('currencies', 'Api\Common\CurrencyController');

Route::post('images/upload', 'Api\Common\ImageController@upload');

/*
|--------------------------------------------------------------------------
| AUTH
|--------------------------------------------------------------------------
|
| Методы для для регистрации, авторизации и выхода из системы
|
*/

Auth::routes(['verify' => true]);

Route::post('auth/signup', 'Api\Auth\RegisterController@register')
    ->name('auth.register');

Route::post('auth/login', 'Api\Auth\LoginController@login')
    ->name('api.auth.login');

Route::get('auth/logout', 'Api\Auth\LoginController@logout')
    ->name('api.auth.logout');

Route::post('auth/leads', 'Api\Auth\LeadController@store');

/*
|--------------------------------------------------------------------------
| CLIENT
|--------------------------------------------------------------------------
|
| Методя для работы на клиенте
|
*/

Route::prefix('client')->name('client.')->group(function (){
    Route::resource('shops', 'Api\Client\ShopController');

    Route::resource('products', 'Api\Client\ProductController');

    Route::resource('currencies', 'Api\Client\CurrencyController');

    Route::resource('orders', 'Api\Client\OrderController');

    Route::resource('users', 'Api\Client\UserController');

    Route::resource('reviews', 'Api\Client\ReviewController');

    Route::prefix('search')->group(function (){
        Route::get('/', 'Api\Client\SearchController@search');
    });
});

/*
|--------------------------------------------------------------------------
| ACQUIRING
|--------------------------------------------------------------------------
|
| Получения статуса платежа
|
*/

Route::prefix('acquiring')->group(function (){
    Route::any('success', 'Api\Acquiring\AcquiringController@success')
        ->name('acquiring.success');

    Route::any('fail', 'Api\Acquiring\AcquiringController@fail')
        ->name('acquiring.fail');
});

/*
|--------------------------------------------------------------------------
| TEST
|--------------------------------------------------------------------------
|
| Методя для работы на клиенте
|
*/

Route::prefix('test')->group(function (){
    Route::get('test', 'Api\TestController@test');
    Route::post('create', 'Api\TestController@create');
});
