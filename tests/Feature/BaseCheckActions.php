<?php


namespace Tests\Feature;


use App\DataModels\Status;
use App\Models\Model;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Query\QueryBuilder as QueryBuilderAlias;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class BaseCheckActions extends TestCase
{
    protected $routes = [
        'index'  => '',
        'show'   => '',
        'store'  => '',
        'update' => '',
        'delete' => ''
    ];

    protected $model;
    protected $responseStructure;

    /**
     * array ['index'] string необязательный, роут на экшен index
     * array ['show'] string необязательно, роут на экшпен show
     * array ['store'] string необязательно, роут на экшен store
     * array ['update'] string необаательно, роут на экшен update
     * array ['delete'] string необязательный, роут на экшен delete
     * @param array $routes
     */
    public function setRoutes($routes = [])
    {
        foreach ($routes as $key => $route) {
            $this->routes[$key] = $route;
        }
    }

    /**
     * @param Model $model
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    /**
     * test action index
     */
    public function testIndex()
    {
        $response = $this->get(route($this->routes['index']));

        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
        $response->assertJsonStructure([
            'success',
            'data',
            'message'
        ]);
    }

    /**
     * test show
     */
    public function testShow()
    {
        $entity    = ($this->getModelQuery())->first();

        if (is_null($entity->id)) throw new \Exception('not found row');

        $response  = $this->get(route($this->routes['show']) . '/' . $entity->id);

        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
        $response->assertJsonStructure([
            'success',
            'data',
            'message'
        ]);
    }


    protected function getModelQuery() : Builder
    {
        $table = (new $this->model)->getTable();
        $query = $this->model::query();

        if (Schema::hasColumn($table, 'active')) {
            $query->where('active', true);
        } elseif (Schema::hasColumn($table, 'status')) {
            $query->where('status', Status::CODE_ACTIVE);
        }

        return $query;
    }
}
