<?php

namespace Tests\Feature\Client;

use App\DataModels\Status;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;
use Tests\Feature\BaseCheckActions;

class ClientProductsTest extends BaseCheckActions
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setRoutes([
            'index' => 'client.products.index',
            'show'  => 'client.products.index'
        ]);

        $this->model = Product::class;
    }

    protected function getModelQuery() : Builder
    {
        $query = $this->model::select('products.*')
            ->where('products.active', true)
            ->join('shops', function ($leftJoin) {
                $leftJoin->on('shops.id', '=', 'products.shop_id')
                    ->where('shops.active', true);
            });
        return $query;
    }
}
