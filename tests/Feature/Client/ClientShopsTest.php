<?php

namespace Tests\Feature\Client;

use App\Models\Shop;
use Tests\Feature\BaseCheckActions;

class ClientShopsTest extends BaseCheckActions
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->setRoutes([
            'index' => 'client.shops.index',
            'show'  => 'client.shops.index'
        ]);

        $this->model = Shop::class;
    }
}
